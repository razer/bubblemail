# Copyright 2019 - 2022 razer <razerraz@free.fr>
# Copyright 2016, 2018 Timo Kankare <timo.kankare@iki.fi>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#

""" Test cases for Mail module """
import os
import sys
import tarfile
import shutil
import email
import pickle
import pytest

sys.path.insert(0, '../bubblemail')
# pylint: disable=C0413, C0415, W0621, W0212, R0914
from bubblemail.config import Config as C
from bubblemail.account import (Account as A, AccountStatus as AS,
                                LocalAccount as LA, RemoteAccount as RA)
from bubblemail.mail import Mail as M

CACHE_DIR = '/tmp/bubblemail-test'
MAIL_STRING = """\
from: "0*/4You" <you@example.org>
To: Me <me@example.org>, Other <me@example.org>
Cc: Third guy <tg@example.org>
subject: Hello...
Date: Tue, 01 May 2018 16:28:08 +0300

...World!
"""

MAIL_TEMPLATE = email.message_from_string(MAIL_STRING)
MAIL_TEMPLATE2 = email.message_from_string(MAIL_STRING.replace('exam', 'toto'))
MAIL_TEMPLATE_NOTO = email.message_from_string(MAIL_STRING.replace(
    'To: Me <me@example.org>, Other <me@example.org>\n', ''))
MAIL_TEMPLATE_NOCC = email.message_from_string(MAIL_STRING.replace(
    'Cc: Third guy <tg@example.org>\n', ''))
MAIL_TEMPLATE_NORC = email.message_from_string(MAIL_STRING.replace(
    'To: Me <me@example.org>, Other <me@example.org>\n'
    +'Cc: Third guy <tg@example.org>\n', ''))
FAKE_HOME = '/tmp/bb_test_fake_home'

class FakeImapBackend:
    def __init__(self, account):
        self.account = account
        self.status = account.status
    @classmethod
    def open(cls):
        return cls
    @staticmethod
    def close():
        return True
    def list_messages(self):
        return [('INBOX', MAIL_TEMPLATE), ('Folder', MAIL_TEMPLATE2)]

class FakeImapBackendTimeout(FakeImapBackend):
    def list_messages(self):
        if self.account[A.NAME] == 'a':
            self.status.set(AS.TIMEOUT)
            return None
        return super().list_messages()

class FakeImapBackendOffline(FakeImapBackend):
    def list_messages(self):
        if self.account[A.NAME] == 'a':
            self.status.set(AS.OFFLINE)
            return None
        return super().list_messages()

@pytest.fixture
def fake_config_folder():
    if os.path.isdir(CACHE_DIR):
        shutil.rmtree(CACHE_DIR)
    return CACHE_DIR

def test_mail_from_raw():
    mail = M.from_raw('MYACCUUID', '', MAIL_TEMPLATE)
    assert mail[M.UUID]
    assert mail[M.DATETIME] == '1525181288'
    assert mail[M.ACCOUNT] == 'MYACCUUID'
    assert mail[M.SUBJECT] == 'Hello...'
    assert mail[M.NAME] == '0*/4You'
    assert mail[M.ADDRESS] == 'you@example.org'
    assert mail[M.RECIPIENTS] == ('Me <me@example.org> Other <me@example.org> '
                                  + 'Third guy <tg@example.org>')
    mail = M.from_raw('MYACCUUID', '', MAIL_TEMPLATE_NOTO)
    assert mail[M.RECIPIENTS] == ('Third guy <tg@example.org>')
    mail = M.from_raw('MYACCUUID', '', MAIL_TEMPLATE_NOCC)
    assert mail[M.RECIPIENTS] == ('Me <me@example.org> Other <me@example.org>')
    mail = M.from_raw('MYACCUUID', '', MAIL_TEMPLATE_NORC)
    assert not mail[M.RECIPIENTS]
    with tarfile.open('tests/fake_mails.tar.xz') as tar_file:
        tar_file.extractall(FAKE_HOME)
    with open(os.path.join(FAKE_HOME, 'bad_mail_raw.txt'), 'rb') as bad_email:
        bad_email = bad_email.read()
    msg = email.message_from_bytes(bad_email)
    mail = M.from_raw('MYACCUUID', '', msg)
    assert mail[M.ADDRESS] == 'sales@e-shop.gr'
    assert mail[M.NAME] == 'e-shop.gr Sales'

    with open(os.path.join(FAKE_HOME, 'bad_mail_raw2.txt'), 'rb') as bad_email:
        bad_email = bad_email.read()
    msg = email.message_from_bytes(bad_email)
    mail = M.from_raw('MYACCUUID', '', msg)
    assert mail[M.ADDRESS] == 'infodsi.noreply@rscn.fr'
    assert mail[M.NAME] == 'CNRS Info-DSI (Ne pas répondre)'
    assert mail[M.SUBJECT] == '[My CoRe]  Retour du service à la normale'

    with open(os.path.join(FAKE_HOME, 'bad_mail_raw3.txt'), 'rb') as bad_email:
        bad_email = bad_email.read()
    msg = email.message_from_bytes(bad_email)
    mail = M.from_raw('MYACCUUID', '', msg)
    assert mail[M.ADDRESS] == 'adminJCMS@mydomain.me'
    assert mail[M.NAME] == 'Firstname Lastname (AcadéDomAin)'
    assert mail[M.SUBJECT] == ('[AcadéDomAin] [Information] Recommandation sur '
                               + '"Coronavirus | Guide de continuité '
                               + 'pédagogique à destination des équipes de '
                               + 'direction et des équipes pédagogiques"')

    with open(os.path.join(FAKE_HOME, 'bad_mail_raw4.txt'), 'rb') as bad_email:
        bad_email = bad_email.read()
    msg = email.message_from_bytes(bad_email)
    mail = M.from_raw('MYACCUUID', '', msg)
    assert mail[M.SUBJECT] == 'αριθμός φορτωτικής 1ZC06F478644083089'

def test_convert():
    assert M.convert(None) == ''
    assert M.convert('') == ''
    assert M.convert(b'bytes are bad') == ''
    assert M.convert(b'\x00\xff') == ''
    assert M.convert(1234) == ''
    assert M.convert(['toto', 14]) == ''
    malformed_text = '=?ISO-8859-1?Q?Hello?='
    assert M.convert(malformed_text) == 'Hello'
    malformed_text = '=?UTF-8?Q?Zg=C5=82oszenie__?='
    assert M.convert(malformed_text) == 'Zgłoszenie  '
    malformed_text = '[My CoRe] Retour =?UTF-8?Q?=C3=A0?= la normale'
    assert M.convert(malformed_text) == '[My CoRe] Retour à la normale'


def test_sort():
    def _replace(old, new):
        return email.message_from_string(MAIL_STRING.replace(old, new))
    oldest_mail = M.from_raw('a', '', MAIL_TEMPLATE)
    old_mail = M.from_raw('a', '', _replace('16:28', '17:28'))
    new_mail = M.from_raw('a', '', _replace('16:28', '19:15'))
    newest_mail = M.from_raw('a', '', _replace('01 May', '02 May'))
    sort_map = [oldest_mail, old_mail, new_mail, newest_mail]
    unsort_map = [new_mail, oldest_mail, newest_mail, old_mail]
    assert M.sort(unsort_map) == sort_map
    assert M.sort(unsort_map, reverse=True) == list(reversed(sort_map))

def test_collect_accounts(monkeypatch, mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    if os.path.exists(FAKE_HOME):
        shutil.rmtree(FAKE_HOME)
    fake_maildir = os.path.join(FAKE_HOME, 'fake_mail')
    os.makedirs(fake_maildir)
    monkeypatch.setenv('HOME', FAKE_HOME)
    for fake_localmail in 'fake_maildir', 'fake_mbox':
        with tarfile.open(f'tests/{fake_localmail}.tar.xz') as tar_file:
            tar_file.extractall(fake_maildir)
    config = C()
    mocker.patch('bubblemail.mail.CACHE_DIR', CACHE_DIR)
    from bubblemail.mail import MailCache
    cache = MailCache()
    account1 = LA(config, name='a', path=os.path.join(fake_maildir, 'Maildir'))
    account2 = LA(config, name='b', path=os.path.join(fake_maildir, '.mbox'))
    account3 = LA(config, name='c', path=os.path.join(fake_maildir, 'Maildir'))
    account2[A.BACKEND] = A.MBOX
    new_mails, unseen_mails = M.collect([account1], cache, first_check=True)
    assert len(new_mails) == 5
    assert 'avatars/E.png' in new_mails[0][M.AVATAR]
    assert 'e-shop.gr Sales' in new_mails[0][M.NAME]
    assert 'avatars/T.png' in new_mails[1][M.AVATAR]
    assert 'Tobacco Store' in new_mails[1][M.NAME]
    assert len(unseen_mails) == 5
    new_mails, unseen_mails = M.collect([account1], cache)
    assert not new_mails
    assert len(unseen_mails) == 5
    new_mails, unseen_mails = M.collect([account1, account2], cache,
                                        first_check=True)
    assert len(new_mails) == 8
    assert len(unseen_mails) == 8
    new_mails, unseen_mails = M.collect([account1, account2], cache)
    assert not new_mails
    assert len(unseen_mails) == 8
    new_mails, unseen_mails = M.collect([account1, account2, account3], cache,
                                        first_check=True)
    assert len(new_mails) == 13
    assert len(unseen_mails) == 13
    new_mails, unseen_mails = M.collect([account1, account2, account3], cache)
    assert not new_mails
    assert len(unseen_mails) == 13


def test_collect_failed_accounts(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = C()
    mocker.patch('bubblemail.mail.CACHE_DIR', CACHE_DIR)
    from bubblemail.mail import MailCache
    cache = MailCache()
    account1 = RA(config, name='a')
    account2 = RA(config, name='b')
    test_mail = M()
    test_mail.BACKEND_MAP[A.IMAP] = FakeImapBackend
    new_mails, unseen_mails = test_mail.collect([account1, account2], cache)
    assert len(new_mails) == 4
    assert len(unseen_mails) == 4
    test_mail.BACKEND_MAP[A.IMAP] = FakeImapBackendTimeout
    new_mails, unseen_mails = test_mail.collect([account1, account2], cache)
    assert len(new_mails) == 0
    assert len(unseen_mails) == 4
    new_mails, unseen_mails = test_mail.collect([account1, account2], cache)
    assert len(new_mails) == 0
    assert len(unseen_mails) == 4
    test_mail.BACKEND_MAP[A.IMAP] = FakeImapBackendTimeout
    new_mails, unseen_mails = test_mail.collect([account1, account2], cache)
    assert len(new_mails) == 0
    assert len(unseen_mails) == 4

def test_mailcache_load(mocker):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    mocker.patch('bubblemail.mail.CACHE_DIR', CACHE_DIR)
    from bubblemail.mail import MailCache
    fake_cache_file = os.path.join(CACHE_DIR, 'mails.cache')
    assert MailCache().load() is None
    with open(fake_cache_file, 'w', encoding='utf-8') as corrupted_file:
        corrupted_file.write('always equal to \n the length\n\n of the string')
    mail_cache = MailCache()
    assert not bool(mail_cache)
    with open(fake_cache_file, 'wb') as corrupted_file:
        pickle.dump({}, corrupted_file)
    assert not bool(mail_cache)
    with open(fake_cache_file, 'wb') as corrupted_file:
        pickle.dump({'7b63c8': False}, corrupted_file)
    mail_cache = MailCache()
    assert not bool(mail_cache)
    with open(fake_cache_file, 'wb') as corrupted_file:
        pickle.dump({'7b63c8': [False, 'eeff00ff']}, corrupted_file)
    mail_cache = MailCache()
    assert not bool(mail_cache)
    cache_content = {
        '7b63c8': [False, {'account': '9dbe2557', 'datetime': '1589320733'}]}
    with open(fake_cache_file, 'wb') as valid_file:
        pickle.dump(cache_content, valid_file)
    mail_cache = MailCache()
    assert mail_cache == cache_content
    assert mail_cache.contains('7b63c8')
    assert not mail_cache.is_dismissed('7b63c8')

def test_mailcache_add_delete(mocker):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    mocker.patch('bubblemail.mail.CACHE_DIR', CACHE_DIR)
    from bubblemail.mail import MailCache
    mail_cache = MailCache()
    mail = M.from_raw('my acc', '', MAIL_TEMPLATE)
    mail_cache.add(mail, dismissed=False)
    assert not mail_cache.is_dismissed(mail[M.UUID])
    assert mail_cache[mail[M.UUID]][1] == mail
    mail_cache.delete(mail[M.UUID])
    mail_cache.add(mail, dismissed=True)
    assert mail_cache.is_dismissed(mail[M.UUID])
    mail_cache.delete(mail[M.UUID])
    assert mail[M.UUID] not in mail_cache
    mail_cache.delete(mail[M.UUID])

def test_mailcache_sync(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    mocker.patch('bubblemail.mail.CACHE_DIR', CACHE_DIR)
    config = C()
    from bubblemail.mail import MailCache
    mail_cache = MailCache()
    account1 = A(config, name='a')
    mail1 = M.from_raw(account1[A.UUID], '', MAIL_TEMPLATE)
    account2 = A(config, name='b')
    mail2 = M.from_raw(account2[A.UUID], '', MAIL_TEMPLATE2)
    mail_cache.add(mail1, dismissed=False)
    mail_cache.add(mail2, dismissed=False)
    mail_cache.sync([account1, account2], [mail1, mail2])
    assert mail_cache.contains(mail1[M.UUID])
    assert mail_cache.contains(mail2[M.UUID])
    assert mail_cache[mail1[M.UUID]][1] == mail1
    assert mail_cache[mail2[M.UUID]][1] == mail2
    mail_cache.sync([account1, account2], [])
    assert not mail_cache.contains(mail1[M.UUID])
    assert not mail_cache.contains(mail2[M.UUID])

def test_mailcache_sync_keep_dismissed(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    mocker.patch('bubblemail.mail.CACHE_DIR', CACHE_DIR)
    config = C()
    from bubblemail.mail import MailCache
    mail_cache = MailCache()
    account1 = A(config, name='a')
    mail1 = M.from_raw(account1[A.UUID], '', MAIL_TEMPLATE)
    mail_cache.add(mail1, dismissed=False)
    mail_cache.sync([account1], [mail1])
    assert mail_cache.contains(mail1[M.UUID])
    mail_cache.sync([account1], [])
    assert not mail_cache.contains(mail1[M.UUID])
    mail2 = M.from_raw(account1[A.UUID], '', MAIL_TEMPLATE2)
    mail_cache.add(mail2, dismissed=True)
    mail_cache.sync([account1], [mail2])
    assert mail_cache.contains(mail2[M.UUID])
    mail_cache.sync([account1], [])
    assert mail_cache.contains(mail2[M.UUID])
    for _ in range(9):
        account1.status.increase_fetch_count()
    mail_cache.sync([account1], [])
    assert not mail_cache.contains(mail2[M.UUID])

def test_mailcache_save(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    mocker.patch('bubblemail.mail.CACHE_DIR', CACHE_DIR)
    config = C()
    from bubblemail.mail import MailCache
    mail_cache = MailCache()
    account1 = A(config, name='a')
    mail1 = M.from_raw(account1[A.UUID], '', MAIL_TEMPLATE)
    account2 = A(config, name='b')
    mail2 = M.from_raw(account2[A.UUID], '', MAIL_TEMPLATE2)
    mail_cache[mail1[M.UUID]] = [False, mail1]
    mail_cache[mail2[M.UUID]] = [True, mail2]
    mail_cache.save()
    mail_cache = MailCache()
    assert mail_cache[mail1[M.UUID]] == [False, mail1]
    assert mail_cache[mail2[M.UUID]] == [True, mail2]
    mail_cache[mail1[M.UUID]][0] = True
    mail_cache[mail2[M.UUID]][0] = False
    mail_cache.save()
    mail_cache = MailCache()
    assert mail_cache[mail1[M.UUID]][0]
    assert not mail_cache[mail2[M.UUID]][0]

def test_mailcache_dismiss(mocker):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    mocker.patch('bubblemail.mail.CACHE_DIR', CACHE_DIR)
    from bubblemail.mail import MailCache
    mail_cache = MailCache()
    mail = M.from_raw('my acc', '', MAIL_TEMPLATE)
    mail_cache.add(mail, dismissed=False)
    assert not mail_cache.is_dismissed(mail[M.UUID])
    mail_cache.dismiss(mail[M.UUID])
    assert mail_cache[mail[M.UUID]][0]
    assert mail_cache.is_dismissed(mail[M.UUID])
