# Copyright 2019 - 2022 razer <razerraz@free.fr>
# Copyright 2016, 2018 Timo Kankare <timo.kankare@iki.fi>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#

"""Test cases for Remotemail."""
import sys
from ssl import SSLError

from test_account import fake_config_folder # pylint: disable=unused-import
# pylint: disable=redefined-outer-name
sys.path.insert(0, '../bubblemail')
# pylint: disable=wrong-import-position
from bubblemail.config import Config as C
from bubblemail.account import (Account as A, RemoteAccount as RA,
                                GnomeOnlineAccount as GOA)
from bubblemail.remotemail import RemoteImap


class FakeImapLib:
    # pylint: disable=unused-argument
    def __init__(self, host='', port=143, timeout=None):
        self.state = 'CONNECTED'
        self.select = lambda: None
        self.close = lambda: None
        self.logout = lambda: None
        self.namespace = lambda: None
        self.list = lambda: None
        self.host = host
        self.port = port
        self.authenticate_loaded = False
        self.login_loaded = False

    def authenticate(self, auth_string, auth, set_status=True):
        self.authenticate_loaded = True
        return 0, None

    def login(self, auth_string, auth, set_status=True):
        self.login_loaded = True
        return 0, None

class FakeImapLibSslError:  # pylint: disable=too-few-public-methods
    def __init__(self, host='', port=143, timeout=None):
        raise SSLError()

class FakeImapLibTimeoutError(FakeImapLib):
    @staticmethod
    def login(*args, **kwargs):
        raise OSError('timed out')

class FakeGoaAccount:  # pylint: disable=too-few-public-methods
    @staticmethod
    def get_password_based():
        return True

class FakeImapLibDotNameSpace(FakeImapLib):
    def __init__(self, host='', port=143, timeout=None):
        super().__init__(host=host, port=port, timeout=timeout)
        self.namespace = lambda: ('OK', [b'(("" ".")) NIL NIL'])

class FakeImapLibSlashNameSpace(FakeImapLib):
    def __init__(self, host='', port=143, timeout=None):
        super().__init__(host=host, port=port, timeout=timeout)
        self.namespace = lambda: ('OK', [b'(("" "/")) NIL NIL'])

class FakeImapLibStarNameSpace(FakeImapLib):
    def __init__(self, host='', port=143, timeout=None):
        super().__init__(host=host, port=port, timeout=timeout)
        self.namespace = lambda: ('OK', [b'(("" "*")) NIL NIL'])

def test_imap_open(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)

    config = C()
    imap_account = RA(config, name='a', type=A.INTERNAL, password='12345')
    imap_mail = RemoteImap(imap_account)
    imap_mail.ssl_backend = FakeImapLibTimeoutError
    imap_mail.open()
    assert imap_account.status.get() == imap_account.status.TIMEOUT

    config = C()
    imap_account = GOA(config, name='a', type=A.GOA, port='143',
                       password='12345')
    imap_account[imap_account.OAUTH2] = 'user=user\1auth=Bearer token\1\1'
    imap_account.goa_account = FakeGoaAccount
    imap_mail = RemoteImap(imap_account)
    imap_mail.backend = FakeImapLib
    imap_mail.open()
    assert imap_account[RA.UNSECURE]
    assert imap_account.status.get() == imap_account.status.SUCCESS
    assert imap_mail.connection.authenticate_loaded

    config = C()
    imap_account = RA(config, name='a', type=A.INTERNAL)
    imap_mail = RemoteImap(imap_account)
    imap_mail.open()
    assert imap_account.status.get() == imap_account.status.CREDENTIALS_MISSING

    config = C()
    imap_account = RA(config, name='a', type=A.INTERNAL, port='143',
                      password='12345')
    imap_mail = RemoteImap(imap_account)
    imap_mail.backend = FakeImapLib
    imap_mail.open()
    assert imap_account[RA.UNSECURE]
    assert imap_account.status.get() == imap_account.status.SUCCESS
    assert imap_mail.connection.login_loaded

    config = C()
    imap_account = RA(config, name='a', type=A.INTERNAL, password='12345')
    imap_mail = RemoteImap(imap_account)
    imap_mail.backend = FakeImapLib
    imap_mail.ssl_backend = FakeImapLibSslError
    imap_mail.open()
    assert imap_mail.ssl_backend == imap_mail.backend

    config = C()
    imap_account = RA(config, name='a', type=A.INTERNAL, password='12345')
    imap_mail = RemoteImap(imap_account)
    imap_mail.backend = FakeImapLibSslError
    imap_mail.ssl_backend = imap_mail.backend
    imap_mail.open()
    assert imap_account.status.get() == imap_account.status.CONNECT_ERROR

def test_imap_get_folder_separator(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = C()
    imap_account = RA(config, name='a', type=A.INTERNAL, password='12345')
    imap_mail = RemoteImap(imap_account)
    imap_mail.ssl_backend = FakeImapLib
    imap_mail.open()
    assert imap_mail.separator == '/'

    imap_mail = RemoteImap(imap_account)
    imap_mail.ssl_backend = FakeImapLibDotNameSpace
    imap_mail.open()
    assert imap_mail.separator == '.'

    imap_mail = RemoteImap(imap_account)
    imap_mail.ssl_backend = FakeImapLibSlashNameSpace
    imap_mail.open()
    assert imap_mail.separator == '/'

    imap_mail = RemoteImap(imap_account)
    imap_mail.ssl_backend = FakeImapLibStarNameSpace
    imap_mail.open()
    assert imap_mail.separator == '*'

def test_imap_request_folders(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = C()
    imap_account = RA(config, name='a', type=A.INTERNAL, password='12345')

    imap_mail = RemoteImap(imap_account)
    imap_mail.ssl_backend = FakeImapLibDotNameSpace
    imap_mail.open()
    imap_mail.connection.list = lambda: ('OK',
        [b'(\\HasChildren \\UnMarked) "." INBOX',
        b'(\\HasNoChildren \\UnMarked) "." Hello',
        b'(\\HasNoChildren \\UnMarked) "." World',
        b'(\\HasNoChildren \\UnMarked) "." Hello.fgH',
        b'(\\HasChildren \\UnMarked) "." "Hello/toto s"',
        b'(\\HasNoChildren \\UnMarked) "." "Hello/toto s.N'])
    result = imap_mail.request_folders()
    assert imap_mail.separator == '.'
    assert result == ['INBOX', 'Hello', 'World', 'Hello.fgH', 'Hello/toto s',
                      'Hello/toto s.N']

    imap_mail = RemoteImap(imap_account)
    imap_mail.ssl_backend = FakeImapLibSlashNameSpace
    imap_mail.open()
    imap_mail.connection.list = lambda: ('OK',
        [b'(\\HasChildren \\UnMarked) "/" INBOX',
        b'(\\HasNoChildren \\UnMarked) "/" Hello',
        b'(\\HasNoChildren \\UnMarked) "/" World',
        b'(\\HasNoChildren \\UnMarked) "/" INBOX/fgH',
        b'(\\HasChildren \\UnMarked) "/" "INBOX/toto s"',
        b'(\\HasNoChildren \\UnMarked) "/" "INBOX/toto s.N',
        b'(\\HasNoChildren \\UnMarked) "/" "INBOX/toto s/titi A',
        b'(\\HasNoChildren \\UnMarked) "/" "Abc Def"'])
    result = imap_mail.request_folders()
    assert result == ['INBOX', 'Hello', 'World', 'INBOX/fgH', 'INBOX/toto s',
                      'INBOX/toto s.N', 'INBOX/toto s/titi A', 'Abc Def']
