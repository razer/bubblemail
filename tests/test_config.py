# Copyright 2016, 2018 Timo Kankare <timo.kankare@iki.fi>
# Copyleft 2019 razer <razerraz@free.fr>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#

""" Test cases for Config module """
import os
import sys
import json
import time
import shutil
from configparser import RawConfigParser #, Error as ConfigParserError
import pytest

sys.path.insert(0, '../bubblemail')
# pylint: disable=C0413
from bubblemail.config import Config as C, PACKAGE_NAME

CONF_DIR = '/tmp/bubblemail-test'
FAKE_ACCOUNT_SECTION = 'Account 2e639951'
FAKE_ACCOUNT_CONFIG = (
        f'[{FAKE_ACCOUNT_SECTION}]\nbackend = imap\nname = T\ntype = internal\n'
        + 'error = \nserver = imap.test.me\nuser = testuser\nenabled = 1\n'
        + 'unsecure = 0\nport = \nuuid = 2e639951\nwebmail = \n')
FAKE_ACCOUNT_FOLDERS = ['Folder A', 'Folder B', 'Folder C']

# pylint: disable=W0621, W0212
@pytest.fixture
def fake_config_folder():
    if os.path.isdir(CONF_DIR):
        shutil.rmtree(CONF_DIR)
    return CONF_DIR

def test_config_generate_new(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = C()
    assert config.is_new
    assert os.path.isfile(config._file)
    config = C()
    assert not config.is_new
    saved_config = RawConfigParser()
    saved_config.read(config._file)
    assert saved_config.has_section(C.CORE)
    for setting in C.CORE_DEFAULTS.items():
        assert saved_config.get(C.CORE, setting[0]) == setting[1]


def test_check_config_version(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    base_version = C().get_core(C.VERSION)
    mocker.patch('bubblemail.config.APP_VERSION', '0.3')
    mocker.patch('bubblemail.config.MIN_COMPAT', 0.2)
    old_config = C(load=False)
    old_config.CORE_DEFAULTS[C.VERSION] = '0.3'
    old_config.load()
    old_config.set_core(C.VERSION, '0.1alphabeta')
    old_config.set_core(C.POLL_INTERVAL, '30')
    old_config.save()
    config = C()
    assert config.get_core(C.VERSION) == '0.3'
    assert config.get_core(C.POLL_INTERVAL) == C.CORE_DEFAULTS[C.POLL_INTERVAL]
    old_config = C(load=False)
    old_config.CORE_DEFAULTS[C.VERSION] = '0.3'
    old_config.load()
    old_config.set_core(C.VERSION, '0.2alphabeta')
    old_config.set_core(C.POLL_INTERVAL, '30')
    old_config.save()
    config = C()
    assert config.get_core(C.VERSION) == '0.3'
    assert config.get_core(C.POLL_INTERVAL) == '30'
    old_config = C(load=False)
    old_config.CORE_DEFAULTS[C.VERSION] = '0.3'
    old_config.load()
    old_config.set_core(C.VERSION, 'badversion')
    old_config.set_core(C.POLL_INTERVAL, '30')
    old_config.save()
    config = C()
    assert config.get_core(C.VERSION) == '0.3'
    assert config.get_core(C.POLL_INTERVAL) == C.CORE_DEFAULTS[C.POLL_INTERVAL]
    C.CORE_DEFAULTS[C.VERSION] = base_version

def test_corrupted_config_file(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = C()
    with open(config._file, 'w', encoding='utf-8') as corrupted_file:
        corrupted_file.write('dlkjgrklj\ngklfjgkfjdkx\ngljdfkjgdfskljg')
    config = C()
    for key, value in C.CORE_DEFAULTS.items():
        assert config.get(C.CORE, key) == value

def test_key_missing_config_file(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = C()
    for key, value in C.CORE_DEFAULTS.items():
        misskey_config = RawConfigParser()
        misskey_config.read(config._file)
        misskey_config.remove_option(C.CORE, key)
        with open(config._file, 'w', encoding='utf-8') as configfile:
            misskey_config.write(configfile)
        config = C()
        assert config.get_core(key) == value

def test_config_load_list_option(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = C()
    fake_account_folders = 'Folder_SPACE_A, Folder_SPACE_B, Folder_SPACE_C'
    with open(config._file, 'w', encoding='utf-8') as config_file:
        config_file.write(FAKE_ACCOUNT_CONFIG)
        config_file.write(f'folders = {fake_account_folders}\n')
    config = C()
    c_folders = config.get_list(FAKE_ACCOUNT_SECTION, 'folders')
    assert c_folders == FAKE_ACCOUNT_FOLDERS
    with open(config._file, 'w', encoding='utf-8') as config_file:
        config_file.write(FAKE_ACCOUNT_CONFIG)
        config_file.write(f'folders = {json.dumps(FAKE_ACCOUNT_FOLDERS)}\n')
    config = C()
    c_folders = config.get_list(FAKE_ACCOUNT_SECTION, 'folders')
    assert c_folders == FAKE_ACCOUNT_FOLDERS

def test_config_save_list_option(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = C()
    with open(config._file, 'w', encoding='utf-8') as config_file:
        config_file.write(FAKE_ACCOUNT_CONFIG)
    config = C()
    config.set(FAKE_ACCOUNT_SECTION, 'folders', FAKE_ACCOUNT_FOLDERS)
    config.save()
    config = C()
    json_folders = config.get(FAKE_ACCOUNT_SECTION, 'folders')
    assert json.loads(json_folders) == FAKE_ACCOUNT_FOLDERS

def test_serialize_config_file(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = C()
    serialized_config = config.serialize()
    assert len(serialized_config) == len(config)
    invalid_sections = filter(lambda s: 'section_name' not in s.keys(),
                              serialized_config)
    assert not list(invalid_sections)
    config_from_serialized = C(load=False)
    config_from_serialized.load_serialized(serialized_config)
    assert config_from_serialized == config

def test_enable_plugin(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = C()
    config.enable_plugin('EnabledPlugin')
    assert 'EnabledPlugin' in config.get(C.CORE, C.ENABLED_PLUGINS)
    config.enable_plugin('EnabledPlugin')
    assert config.get(C.CORE, C.ENABLED_PLUGINS).count('EnabledPlugin') == 1

def test_disable_plugin(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config = C()
    config.enable_plugin('PluginToDisable1')
    config.enable_plugin('PluginToDisable2')
    config.disable_plugin('PluginToDisable1')
    assert 'PluginToDisable1' not in config.get(C.CORE, C.ENABLED_PLUGINS)
    assert config.get(C.CORE, C.ENABLED_PLUGINS).count('PluginToDisable2') == 1
    assert not config.get(C.CORE, C.ENABLED_PLUGINS)[0].startswith(' ')
    assert not config.get(C.CORE, C.ENABLED_PLUGINS)[0].endswith(' ')
    config.disable_plugin('PluginToDisable2')
    assert 'PluginToDisable2' not in config.get(C.CORE, C.ENABLED_PLUGINS)
    assert ' ' not in config.get(C.CORE, C.ENABLED_PLUGINS)

def test_saving_bypass(mocker, fake_config_folder):
    mocker.patch('bubblemail.config.CONFIG_DIR', fake_config_folder)
    config_file = os.path.join(CONF_DIR, f'{PACKAGE_NAME}.cfg')
    config = C()
    last_mutation = os.path.getmtime(config_file)
    config.save()
    assert os.path.getmtime(config_file) == last_mutation
    time.sleep(.1)
    config.set(C.CORE, C.POLL_INTERVAL, '9')
    config.save()
    assert os.path.getmtime(config_file) != last_mutation
    last_mutation = os.path.getmtime(config_file)
    time.sleep(.1)
    config.save()
    assert os.path.getmtime(config_file) == last_mutation
    config.add_section('Account 12345678')
    time.sleep(.1)
    config.save()
    assert os.path.getmtime(config_file) != last_mutation
    last_mutation = os.path.getmtime(config_file)
    time.sleep(.1)
    config.save()
    assert os.path.getmtime(config_file) == last_mutation
    config.remove_section('Account 12345678')
    time.sleep(.1)
    config.save()
    assert os.path.getmtime(config_file) != last_mutation
    last_mutation = os.path.getmtime(config_file)
    time.sleep(.1)
    config.save()
    assert os.path.getmtime(config_file) == last_mutation
