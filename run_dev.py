#!/usr/bin/env python3

# Copyright 2019 - 2022 razer <razerraz@free.fr>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.

import os
import builtins
import argparse

builtins.BB_DEVMODE = True
builtins.BB_DEVPATH = os.path.dirname(os.path.realpath(__file__))
PACKAGE_NAME = 'bubblemail'

PARSER = argparse.ArgumentParser(
    prog=PACKAGE_NAME, description=f'Run {PACKAGE_NAME} components in dev mode')
PARSER.add_argument('-d', '--service', action='store_true', default=True,
                    help='Start service (default)')
PARSER.add_argument('-s', '--settings', action='store_true',
                    help='Show settings window')
PARSER.add_argument('-r', '--rebuild', action='store_true',
                    help='Rebuild components')
PARSER.add_argument('-f', '--homefolder',
                    help='Folder used to store config & cache (default is ~)')

ARGS = PARSER.parse_args()
if ARGS.homefolder:
    print(f'Using home folder {ARGS.homefolder}')
    builtins.BB_DEVHOMEPATH = ARGS.homefolder

# pylint: disable=C0412, C0413
from build_tools import (build_avatar_provider, create_default_avatars,
                         gen_locales)
gen_locales(force=ARGS.rebuild)

if ARGS.settings:
    from bubblemail.ui.settingswindow import SettingsWindow
    SettingsWindow()
else:
    build_avatar_provider(force=ARGS.rebuild)
    create_default_avatars(force=ARGS.rebuild)
    from bubblemail.service import Service
    Service().run()
