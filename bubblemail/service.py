# Copyright 2019 - 2022 razer <razerraz@free.fr>
# Copyright 2014, 2015 Patrick Ulbrich <zulu99@gmx.net>
# Copyright 2016 Timo Kankare <timo.kankare@iki.fi>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#
import sys
import threading
import logging
import signal
import time
from datetime import datetime, timedelta
import importlib
import dbus
from dbus.mainloop.glib import DBusGMainLoop
from gi.repository import GLib

from .utils import (init_logging, shutdown_service, set_log_level)
                    # version_checker)
from .account import Account as A, GnomeOnlineAccount
from .mail import Mail, MailCache
from .localmail import LocalMail
from .dbusservice import DBusService
from .plugin import PluginAgregator
from .config import Config, PACKAGE_NAME #, APP_VERSION , RELEASE_URL

INITIAL_REFRESH_DELAY = 10

RUNNING = 'running'
STOPPED = 'stopped'
LAST_CHECK = 'last_check'
MAIN = 'main'
LOOP = 'loop'
STOP_LOOP = 'stop_poll'

NM_CONNECTED = 70
NM_NAME = 'org.freedesktop.NetworkManager'
LM_NAME = 'org.freedesktop.login1'

DBUS_PROPS_NAME = 'org.freedesktop.DBus.Properties'

DBusGMainLoop(set_as_default=True)


class Service:
    def __init__(self):
        self.dbus_service = None
        self.config = None
        self.cache = None
        self.plugins = None
        self.on_refresh = False
        self.refresh_thread = None
        self.refresh_timer = None
        self.last_refresh = None
        self.online = False
        self.running = False
        self.last_unseen_count = 0
        self.status_map = []
        init_logging()

    @property
    def poll_interval(self):
        return int(self.config.get_core(self.config.POLL_INTERVAL))

    def _refresh_by_handler(self):
        if self.last_refresh + timedelta(seconds=20) > datetime.now():
            logging.info('Avoiding too early refresh , last refresh: %s',
                         self.last_refresh)
            return
        threading.Thread(target=self.refresh, kwargs={'wait': 10}).start()

    def _nm_change_handler(self, state):
        """ Handler triggered by NetworkManager on connection changes """
        if self.online == (state == NM_CONNECTED):
            return
        self.online = state == NM_CONNECTED
        logging.debug('Connect state is now %s', self.online)
        if self.online:
            self._refresh_by_handler()

    def _lm_change_handler(self, state):
        """ Handler triggered by systemd on sleep/resume """
        if state:
            logging.debug('Prepare for sleep')
            self.set_refresh_timer(clear=True)
        else:
            logging.debug('Resuming from sleep')
            self._refresh_by_handler()

    def run(self):
        """ Initializes the service and starts checking threads. """
        # Shutdown running instances if any
        shutdown_service()
        self.dbus_service = DBusService(self)
        self.config = Config(dbus_service=self.dbus_service,
                             on_saved_cb=self.on_config_saved)
        set_log_level(self.config)
        if not importlib.util.find_spec('socks'):
            logging.warning(
                'pysocks module not found, proxy support will be disabled')
        # Check for new version
        # self.config.set_core(Config.UPDATE_AVAILABLE,
        #                      version_checker(RELEASE_URL, APP_VERSION))
        self.cache = MailCache()
        bus = dbus.SystemBus()
        # Stop everything properly on kill
        for stop_signal in signal.SIGINT, signal.SIGTERM:
            GLib.unix_signal_add(GLib.PRIORITY_HIGH, stop_signal, self.stop)
        # Connect to networkmanager connect state signal
        if bus.name_has_owner(NM_NAME):
            proxy = bus.get_object(NM_NAME, '/' + NM_NAME.replace('.', '/'))
            proxy.connect_to_signal('StateChanged', self._nm_change_handler)
        # Connect so systemd login manager for catching sleep/resume signals
        if bus.name_has_owner(LM_NAME):
            proxy = bus.get_object(LM_NAME, '/' + LM_NAME.replace('.', '/'))
            proxy.connect_to_signal('PrepareForSleep', self._lm_change_handler)
        # Load plugins
        self.plugins = PluginAgregator(service=self)
        self.plugins.enable()
        self.running = True
        refresh_args={'first_check': True, 'wait': INITIAL_REFRESH_DELAY}
        if self.config.is_new:
            LocalMail.search_paths(self.config)
            refresh_args['wait'] = None
        threading.Thread(target=self.refresh, kwargs=refresh_args).start()
        logging.debug(f'{PACKAGE_NAME.capitalize()} service ready')
        GLib.MainLoop().run()

    def stop(self, unused_signum=None, unused_frame=None):
        if not self.running:
            return
        logging.info('Stopping service')
        self.running = False
        if self.refresh_timer:
            self.refresh_timer.cancel()
        logging.debug('Stopping GLib loop')
        GLib.MainLoop().quit()
        logging.info('Bye...')
        sys.exit(0)

    def set_refresh_timer(self, clear=False):
        if clear:
            if self.refresh_timer:
                self.refresh_timer.cancel()
                self.refresh_timer = None
            return
        self.refresh_timer = threading.Timer(60*self.poll_interval or 180,
                                             self.refresh)
        self.refresh_timer.start()

    def refresh(self, first_check=False, force=False, wait=None):
        """ Check local and remote accounts for new mails
            If first_check, consider all unreaded mails as new ones
            If force, check even if paused by user
            If wait, insert sleep with specified value
        """
        if wait:
            time.sleep(wait)
        if self.on_refresh:
            logging.info('A collect is already running')
            return
        if not force and not self.poll_interval:
            logging.info('Auto refresh disabled in config')
            self.set_refresh_timer()
            return
        # Reset refresh timer (in case refresh haven't been triggered by it)
        self.set_refresh_timer(clear=True)
        self.on_refresh = True
        self.last_refresh = datetime.now()
        if GnomeOnlineAccount.is_available():
            GnomeOnlineAccount.sync(self.config)
        else:
            if first_check:
                logging.warning('No gnome online accounts support found')
            GnomeOnlineAccount.cleanup(self.config)
        # ON_COLLECT plugin hook triggering
        accounts = self.plugins.run_hook('on_collect',
                                         list(A.list(self.config)))
        if not accounts or not list(filter(lambda a: a[A.ENABLED], accounts)):
            logging.info('No enabled account found, bypass collect')
            self.on_refresh = False
            self.set_refresh_timer()
            return
        # Collect mails
        new_mails, unseen_mails = Mail.collect(
            accounts, self.cache, first_check=first_check)
        # Force on_config_saved plugin hook triggering on first run
        if first_check:
            self.on_config_saved()
        # Get account status
        enabled_accounts = filter(lambda a: a[A.ENABLED], accounts)
        self.status_map = list(map(lambda a: a.status, enabled_accounts))
        mutated_map = list(filter(lambda s: s.have_changed, self.status_map))
        if mutated_map:
            serialized_status_map = list(map(lambda s: s.serialized,
                                             self.status_map))
            self.plugins.run_hook('on_status_changed', serialized_status_map)
            self.dbus_service.status_update(serialized_status_map)
        for status in mutated_map:
            status.save()
        if new_mails or self.last_unseen_count != len(unseen_mails):
            # ON_UPDATE plugin hook triggering
            new_mails, unseen_mails = self.plugins.run_hook(
                'on_update', [new_mails, unseen_mails])
            self.dbus_service.on_update(unseen_mails)
        self.last_unseen_count = len(unseen_mails)
        self.on_refresh = False
        self.set_refresh_timer()

    def on_config_saved(self):
        if self.plugins:
            self.config = self.plugins.run_hook('on_config_saved', self.config)
