# Copyright 2019 - 2022 razer <razerraz@free.fr>
# Copyright 2013 - 2016 Patrick Ulbrich <zulu99@gmx.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.

import logging
import threading
import time
import dbus
import dbus.service
from .config import DBUS_BUS_NAME, DBUS_OBJ_PATH
from .utils import set_log_level

MAX_INT32 = ((0xFFFFFFFF / 2) - 1)
STATUS_UPDATE_SIG = 'StatusUpdate'
CFG_UPDATE_SIG = 'ConfigUpdate'
CFG_SET_METHOD = 'SetConfig'
CFG_GET_METHOD = 'GetConfig'
REFRESH_METHOD = 'Refresh'
PLGSTATE_GET_METHOD = 'GetPluginState'
PLGDISABLE_SET_METHOD = 'DisablePlugin'
PLGENABLE_SET_METHOD = 'EnablePlugin'

class DBusService(dbus.service.Object):
    """  DBUS server with signals and methods """

    # pylint: disable=C0103, W0212
    def __init__(self, service):
        self.service = service
        self.mail_map = []
        bus_name = dbus.service.BusName(DBUS_BUS_NAME, bus=dbus.SessionBus())
        dbus.service.Object.__init__(self, bus_name, DBUS_OBJ_PATH)

    def on_update(self, mail_map):
        self.mail_map = mail_map
        self.ContentUpdate(self.mail_map)

    def status_update(self, status_map):
        self.StatusUpdate(status_map)

    @dbus.service.signal(dbus_interface=DBUS_BUS_NAME, signature='aa{ss}')
    def ContentUpdate(self, mail_map):
        pass

    @dbus.service.signal(dbus_interface=DBUS_BUS_NAME, signature='aa{sv}')
    def StatusUpdate(self, status_map):
        pass

    @dbus.service.signal(dbus_interface=DBUS_BUS_NAME, signature='aa{ss}')
    def ConfigUpdate(self, serialized_config):
        pass

    @dbus.service.method(dbus_interface=DBUS_BUS_NAME, out_signature='aa{ss}')
    def GetConfig(self):
        return self.service.config.serialize()

    @dbus.service.method(dbus_interface=DBUS_BUS_NAME, out_signature='aa{sv}')
    def GetStatus(self):
        # return [{'toto':1, 'titi':'Hello'}, {'toto':1, 'titi':'Hello'}]
        return list(map(lambda s: s.serialized, self.service.status_map))

    @dbus.service.method(dbus_interface=DBUS_BUS_NAME, in_signature='aa{ss}')
    def SetConfig(self, serialized_config):
        def _thread():
            self.service.config.load_serialized(serialized_config)
            self.service.config.save()
            # Reload plugins
            self.service.plugins.reload()
            # Logging level
            set_log_level(self.service.config)
        threading.Thread(target=_thread).start()

    @dbus.service.method(dbus_interface=DBUS_BUS_NAME, out_signature='as')
    def GetPluginState(self):
        if not self.service.plugins.error:
            return []
        return [self.service.plugins.error]

    @dbus.service.method(dbus_interface=DBUS_BUS_NAME, out_signature='aa{ss}')
    def GetContent(self):
        return self.mail_map

    @dbus.service.method(dbus_interface=DBUS_BUS_NAME, in_signature='s')
    def DisablePlugin(self, plugin):
        self.service.config.disable_plugin(plugin)
        self.service.plugins.reload()

    @dbus.service.method(dbus_interface=DBUS_BUS_NAME, in_signature='s')
    def EnablePlugin(self, plugin):
        logging.debug(f'Enabling plugin {plugin}')
        self.service.config.enable_plugin(plugin)
        self.service.plugins.reload()

    @dbus.service.method(dbus_interface=DBUS_BUS_NAME)
    def Refresh(self):
        threading.Thread(target=self.service.refresh,
                         kwargs={'force': True}).start()

    @dbus.service.method(dbus_interface=DBUS_BUS_NAME, in_signature='s')
    def Dismiss(self, uuid):
        if not list(filter(lambda m: m[m.UUID] == uuid, self.mail_map)):
            logging.warning(f'Dismiss request for unknown mail uuid "{uuid}" !')
            return
        self.service.cache.dismiss(uuid)
        self.mail_map = list(filter(lambda m: m[m.UUID] != uuid, self.mail_map))

    @dbus.service.method(dbus_interface=DBUS_BUS_NAME)
    def DismissAll(self):
        for mail in self.mail_map:
            self.Dismiss(mail[mail.UUID])

    @dbus.service.method(dbus_interface=DBUS_BUS_NAME)
    def Shutdown(self):
        time.sleep(1)
        self.service.stop()
