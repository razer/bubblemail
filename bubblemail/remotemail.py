# Copyright 2019 - 2022 razer <razerraz@free.fr>
# Copyright 2016 Timo Kankare <timo.kankare@iki.fi>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.

"""Backends to implement mail box specific functionality, like IMAP and POP3."""

from abc import ABCMeta, abstractmethod
import re
import email
import logging
import imaplib
import poplib
from ssl import SSLError
from os import path
import errno
try:
    import socks
except ModuleNotFoundError:
    socks = None  # pylint: disable=invalid-name
import socket

from .account import RemoteAccount as RA, AccountStatus as AS
from .utils import utf7_decode, utf7_encode

IMAP = 'imap'
POP3 = 'pop3'
UNSECURE_PORTS = ('110', '143', '1110', '1143')
DEFAULT_TIMEOUT = 15
BACKEND_MAP = {IMAP: imaplib.IMAP4, POP3: poplib.POP3}
SSL_BACKEND_MAP = {IMAP: imaplib.IMAP4_SSL, POP3: poplib.POP3_SSL}


class RemoteMail:
    """Interface for mailbox backends.
    Mailbox backend implements access to the specific type of mailbox.
    """

    __metaclass__ = ABCMeta

    def __init__(self, account):
        self.account = account
        self.backend = BACKEND_MAP[self.account[RA.BACKEND]]
        self.ssl_backend = SSL_BACKEND_MAP[self.account[RA.BACKEND]]
        self.connection = None
        self.err_str = f'{self.account[RA.NAME]}({self.account[RA.SERVER]}):'
        self.set_proxy()
        socket.setdefaulttimeout(DEFAULT_TIMEOUT)

    def _safe_call(self, command, *args, **kwargs):
        """ Try / except wrapper for remote commands and its arguments
            Filter out 'set_status' and 'exc_info' from keyword args
            for internal purpose
            Set account status on error if 'set_status' is True
            Log execution info on error if 'exc_info' is True
        """
        set_status = True
        exc_info = True
        if 'set_status' in kwargs:
            set_status = kwargs['set_status']
            kwargs.pop('set_status')
        if 'exc_info' in kwargs:
            exc_info = kwargs['exc_info']
            kwargs.pop('exc_info')

        def set_account_status(status):
            if set_status:
                self.account.status.set(status)
            return status

        try:
            return AS.SUCCESS, command(*args, **kwargs)
        except socket.timeout:
            logging.warning(f'{self.err_str} timeout')
            return set_account_status(AS.TIMEOUT), None
        except OSError as oserror:
            if 'timed out' in str(oserror):
                logging.warning(f'{self.err_str} timeout')
                return set_account_status(AS.TIMEOUT), None
            logging.error(f'{self.err_str} unknown error', exc_info=exc_info)
            return set_account_status(AS.UNKNOWN), None
        except imaplib.IMAP4.error:
            return set_account_status(AS.CONNECT_ERROR), None
        except Exception:
            logging.error(f'{self.err_str} unknown error', exc_info=exc_info)
            return set_account_status(AS.UNKNOWN), None

    @staticmethod
    def _response_is_valid(remote_response, expected=(list, tuple), minlen=2):
        if not remote_response or not isinstance(remote_response, expected):
            return False
        if len(remote_response) < minlen:
            return False
        return True

    def set_proxy(self):
        if not socks:
            return
        if not self.account[RA.PROXY]:
            socks.set_default_proxy()
            return
        if not (self.account[RA.PROXY_HOST] and self.account[RA.PROXY_PORT]):
            logging.error(
                f'Proxy on {self.account[RA.NAME]}: missing host or port')
        proxy_type = socks.PROXY_TYPE_SOCKS5
        if self.account[RA.PROXY_TYPE] == '4':
            proxy_type = socks.PROXY_TYPE_SOCKS4
        logging.info(
            f'{self.account[RA.NAME]}: using socks{self.account[RA.PROXY_TYPE]}'
            + f' proxy {self.account[RA.PROXY_HOST]} on port '
            + self.account[RA.PROXY_PORT])
        username = None
        if RA.PROXY_USER in self.account.keys() and self.account[RA.PROXY_USER]:
            username = self.account[RA.PROXY_USER]
        password = None
        if RA.PROXY_PASS in self.account.keys() and self.account[RA.PROXY_PASS]:
            password = self.account[RA.PROXY_PASS]
        socks.set_default_proxy(proxy_type, self.account[RA.PROXY_HOST],
                                int(self.account[RA.PROXY_PORT]),
                                username=username, password=password)
        if self.account[RA.BACKEND] == RA.IMAP:
            socks.wrap_module(imaplib)
        elif self.account[RA.BACKEND] == RA.POP3:
            socks.wrap_module(poplib)

    def open(self, timeout=DEFAULT_TIMEOUT):  # pylint: disable=R0912, R0915
        """ Opens the mailbox
        """
        if self.account.is_locked:
            logging.warning(f'Account {self.account[RA.NAME]}: avoiding '
                            + 'connect attempt without credentials !!!')
            self.account.status.set(AS.CREDENTIALS_MISSING)
            return None
        account_name = self.account[RA.NAME]
        server = self.account[RA.SERVER]
        port = self.account[RA.PORT]
        if self.connection:
            logging.warning(f'{account_name}({self.account[RA.TYPE]}): '
                            + f'connection on {server} is already open')
            return self.connection

        def _open(backend, server, port):
            socket.setdefaulttimeout(timeout)
            if port:
                self.connection = backend(server, port)
            else:
                self.connection = backend(server)
            socket.setdefaulttimeout(DEFAULT_TIMEOUT)

        if port in UNSECURE_PORTS or self.account[RA.UNSECURE]:
            # Bypass SSL attempts if specified port is an unsecured one
            logging.debug(f'Bypassing SSL connection attempt on {account_name}')
            self.ssl_backend = self.backend
            self.account[RA.UNSECURE] = True

        while True:
            try:
                _open(self.ssl_backend, server, port)
            except SSLError:
                if self.ssl_backend != self.backend:
                    self.ssl_backend = self.backend
                    continue
                status = AS.CONNECT_ERROR
                error = SSLError.strerror
                self.account.status.set(status, error)
            except ConnectionRefusedError:
                self.account.status.set(AS.BAD_LOGIN)
            except socket.timeout:
                self.account.status.set(AS.TIMEOUT)
            except (socket.gaierror, OSError) as oserror:
                status = AS.OFFLINE
                error = None
                if oserror.errno == -2 and not self.account[RA.TYPE] == RA.GOA:
                    status = AS.FAILED
                    error = oserror.strerror
                elif oserror.errno not in (-3, errno.ENETDOWN,
                                           errno.ENETUNREACH):
                    status = AS.CONNECT_ERROR
                    error = oserror.strerror
                self.account.status.set(status, error)
            except Exception:
                logging.error('Unhandled exception', exc_info=True)
                self.account.status.set(AS.UNKNOWN)
            else:
                self.account[RA.UNSECURE] = (
                    port in UNSECURE_PORTS or self.backend == self.ssl_backend)
                return self.connection
            log_func = logging.error
            if self.account.status == AS.OFFLINE:
                log_func = logging.info
            log_func(
                f'{self.account[RA.BACKEND]} server {server} from '
                + f'account {account_name}: '
                + f'{self.account.status.error_message}')
            self.connection = None
            return self.connection

    def decode_message(self, message):
        try:
            return email.message_from_bytes(message)
        except Exception:
            logging.error(
                f'Decoding email from {self.account[RA.NAME]}: {message}',
                exc_info=True)
        return None

    @abstractmethod
    def close(self):
        """ Closes the mailbox.
        """
        if not self.connection:
            return None
        return True

    @abstractmethod
    def is_open(self):
        """ Returns true if mailbox is open.
        """
        raise NotImplementedError

    @abstractmethod
    def list_messages(self):
        """ Lists unseen messages from the mailbox for this account.
            Yields tuples (folder, message) for every message.
            Return None on error
            Be aware that return value need to be iterable
        """
        self.connection = self.connection if self.connection else self.open()
        if not self.connection or not self.is_open():
            return None
        return True

    @abstractmethod
    def request_folders(self):
        """ Returns list of folder names available in the mailbox.
            Raises an exception if mailbox does not support folders.
        """
        raise NotImplementedError


class RemoteImap(RemoteMail):
    """ Implementation of IMAP mail boxes.
    """
    MISSING_FOLDER = -1
    def __init__(self, account):
        super().__init__(account)
        self.separator = '/'

    def open(self, timeout=DEFAULT_TIMEOUT):
        if not super().open(timeout=timeout):
            return None
        if RA.OAUTH2 in self.account and self.account[RA.OAUTH2]:
            status, _unused = self._safe_call(self.connection.authenticate,
                                              'XOAUTH2',
                                              lambda x: self.account[RA.OAUTH2],
                                              set_status=False)
        else:
            status, _unused = self._safe_call(self.connection.login,
                                              self.account[RA.USER],
                                              self.account[RA.PASS],
                                              set_status=False)
        if status != AS.SUCCESS:
            if status == AS.CONNECT_ERROR:
                self.account.status.set(AS.BAD_LOGIN)
            else:
                self.account.status.set(status)
            return None
        self.connection.utf8_enabled = True
        self.account.status.set_success()
        status, response = self._safe_call(self.connection.namespace,
                                           set_status=False, exc_info=False)
        response_is_valid = self._response_is_valid(response, minlen=1)
        if status == AS.SUCCESS and response_is_valid and response[0] == 'OK':
            try:
                self.separator = str(response[1]).split()[1][1]
                logging.debug(f'{self.account[RA.SERVER]}: Folder separator is '
                              + self.separator)
            except (IndexError, TypeError):
                logging.warning(f'{self.account[RA.SERVER]}: can\'t get '
                                + 'folder separator caracter from server, using'
                                + ' default dot', exc_info=True)
        return self.connection

    def close(self):
        if not super().close():
            return
        for method in 'select', 'close', 'logout':
            status, _unused = self._safe_call(getattr(self.connection, method),
                                              set_status=False, exc_info=False)
            if status != AS.SUCCESS:
                break
        self.connection = None

    def is_open(self):
        return self.connection and self.connection.state != 'LOGOUT'

    def select_folder(self, folder):
        _unused, response = self._safe_call(
            self.connection.select, f'"{utf7_encode(folder)}"', readonly=True)
        if self.account.status.timeout:
            return AS.TIMEOUT
        response_is_valid = self._response_is_valid(response, minlen=1)
        if self.account.status.error or not response_is_valid:
            return AS.UNKNOWN
        if response[0] != 'OK':
            return self.MISSING_FOLDER
        return AS.SUCCESS

    def fetch(self, folder, num):
        result = []
        _unused, response = self._safe_call(
            self.connection.fetch, num, '(BODY.PEEK[HEADER])')
        if self.account.status.timeout:
            return None
        if self.account.status.error or not self._response_is_valid(response):
            return result
        for part in response[1]:
            if (not isinstance(part, (tuple, list)) or len(part) < 2
                    or not isinstance(part[1], (bytes, bytearray))):
                continue
            message = self.decode_message(part[1])
            if message:
                result.append((folder, message))
        return result

    def list_messages(self):
        if not super().list_messages():
            return None
        mail_map = []
        folder_map = ['INBOX']
        missing_folders = []
        if RA.FOLDERS in self.account and self.account[RA.FOLDERS]:
            folder_map = map(path.split, self.account[RA.FOLDERS])
            folder_map = map(lambda f: filter(None, f), folder_map)
            folder_map = list(map(self.separator.join, folder_map))
        for folder in folder_map:
            status = self.select_folder(folder)
            if status == AS.TIMEOUT:
                break
            if status == self.MISSING_FOLDER and folder.upper() != 'INBOX':
                missing_folders.append(folder)
            if self.account.status.error:
                continue
            _unused, response = self._safe_call(self.connection.search,
                                                None, 'UNSEEN')
            if self.account.status.timeout:
                mail_map = []
                break
            if self.account.status.error:
                continue
            if not self._response_is_valid(response):
                logging.error(
                    f'Malformed response from {self.account[RA.SERVER]}')
                continue
            result, data = response
            if result != 'OK' or data == b'' or not data[0]:
                logging.debug(f'Folder:{folder}, status:{status}, data:{data}')
                continue # Bugfix LP-735071
            for num in data[0].split():
                fetch_result = self.fetch(folder, num)
                if fetch_result is None:
                    # Timeout: cancel collect
                    return []
                mail_map += fetch_result
        if missing_folders and self.account[RA.FOLDERS]:
            logging.warning(f'Folder(s) {missing_folders} missing, removing')
            folder_dict = dict(zip(folder_map, self.account[RA.FOLDERS]))
            for folder in missing_folders:
                self.account[RA.FOLDERS].remove(folder_dict[folder])
            self.account.save(create=False)

        return mail_map

    def request_folders(self):
        self.connection = self.connection if self.connection else self.open()
        if not self.is_open():
            return []
        _unused, response = self._safe_call(self.connection.list)
        if self.account.status.error or not self._response_is_valid(response):
            return []
        self.close()
        def append_folder(imap_response_item):
            match = re.match(r'.+\s+("."|"?NIL"?)\s+"?([^"]+)"?$',
                             imap_response_item.decode('utf-8'))
            if not match:
                logging.warning(f'{self.account[RA.SERVER]}: bad folder format '
                                + f'from {imap_response_item}')
                return None
            return utf7_decode(match.group(2))

        return list(filter(None, map(append_folder, response[1])))


class RemotePop3(RemoteMail):
    """ Implementation of POP3 mail boxes.
    """
    def open(self, timeout=DEFAULT_TIMEOUT):
        if not super().open(timeout=timeout):
            return None
        try:
            self.connection.user(self.account[RA.USER])
            self.connection.pass_(self.account[RA.PASS])
        except poplib.error_proto:
            logging.error(f'{self.err_str} bad credentials')
            self.account.status.set(AS.BAD_LOGIN)
        except socket.timeout:
            logging.error(f'{self.err_str} timeout')
            self.account.status.set(AS.TIMEOUT)
        except Exception:
            logging.error(f'{self.err_str} unhandled error', exc_info=True)
            self.account.status.set(AS.CONNECT_ERROR)
        else:
            self.account.status.set_success()
            return self.connection
        self.close()
        return None

    def close(self):
        if not super().close():
            return
        self._safe_call(self.connection.quit, set_status=False, exc_info=False)
        self.connection = None

    def is_open(self):
        return self.connection and 'sock' in self.connection.__dict__

    def list_messages(self):
        if not super().list_messages():
            return None
        folder = ''
        mail_map = []
        _unused, response = self._safe_call(self.connection.list)
        if self.account.status.error:
            return None
        if not self._response_is_valid(response) or not response[1]:
            return None
        mail_count = len(response[1])
        for i in range(mail_count):
            _unused, response = self._safe_call(self.connection.top, i+1, 0)
            if self.account.status.timeout:
                return None
            if self.account.status.error:
                continue
            if not self._response_is_valid(response) or not response[1]:
                continue
            message = self.decode_message(b'\n'.join(response[1]))
            if message:
                mail_map.append((folder, message))
        return mail_map

    def request_folders(self):
        raise NotImplementedError('POP3 does not support folders')
