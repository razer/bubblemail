# Copyright 2019 - 2022 razer <razerraz@free.fr>
# Copyright 2013 - 2016 Patrick Ulbrich <zulu99@gmx.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#

import os
from copy import deepcopy
import threading
import logging  # pylint: disable = W0611
import gi
# pylint: disable=C0413
gi.require_version('Notify', '0.7')
gi.require_version('GLib', '2.0')
gi.require_version('Gtk', '3.0')
from gi.repository import Gio
try:
    from gi.repository import Notify
except ModuleNotFoundError:
    Notify = None  # pylint: disable = C0103
import dbus

from bubblemail.config import PACKAGE_NAME
from bubblemail.account import Account as A
from bubblemail.mail import Mail as M
from bubblemail.i18n import _
from bubblemail.plugin import Plugin

ICON_DEFAULT = 'mail-message-new'
DEFAULT_MAXLEN = 60

class Notification:
    def __init__(self, summary, body, icon=ICON_DEFAULT, mail_map=None):
        self.summary = summary
        self.body = body
        self.icon = icon
        self.mail_map = mail_map
        self.action_text = _('Dismiss')
        if self.mail_map and len(self.mail_map) > 1:
            self.action_text = _('Dismiss all')
        self.thread_lock = threading.Lock()
        self.notification = None
        self.actions = None
        self.webmail = None

    @classmethod
    def notify_cap(cls, cap):
        return cap in Notify.get_server_caps()

    def create(self, actions=None, webmail=None, is_error=False):
        self.actions = actions
        self.webmail = webmail
        if self.notify_cap('icon-static'):
            self.notification = Notify.Notification.new(self.summary, self.body,
                                                        self.icon)
        else:
            self.notification = Notify.Notification.new(self.summary, self.body)
        self.notification.set_category('email')
        self.notification.set_hint_string('desktop-entry', PACKAGE_NAME)
        if self.notify_cap('actions'):
            if is_error:
                self.notification.add_action(
                    'open_settings', _('Open settings'),
                    self.action_handler, None)
            else:
                self.notification.add_action('default', 'default',
                                             self.action_handler, self.mail_map)
                self.notification.add_action('dismiss', self.action_text,
                                             self.action_handler, self.mail_map)
        self.notification.show()

    def close(self):
        if not self.notification:
            return
        with self.thread_lock:
            self.notification.close()
            self.notification = None

    def action_handler(self, _, action, mails):
        with self.thread_lock:
            if action == 'default' and not self.webmail:
                mailer = Gio.AppInfo.get_default_for_type(
                    'x-scheme-handler/mailto', False)
                if mailer and hasattr(mailer, 'launch'):
                    mailer.launch()
            elif action == 'default':
                browser = Gio.AppInfo.get_default_for_type(
                    'x-scheme-handler/http', False)
                if browser and hasattr(browser, 'launch_uris'):
                    browser.launch_uris([self.webmail])
            elif action == 'dismiss':
                if not mails:
                    logging.error('Dismiss action handler : no mail list !')
                    return
                for mail in mails:
                    self.actions['dismiss'](mail[M.UUID])
            elif action == 'open_settings':
                Gio.DesktopAppInfo.new(f'{PACKAGE_NAME}.desktop').launch()
        self.close()

class LibNotifyPlugin(Plugin):
    MANIFEST = (
        _('LibNotify Notifications'), _('Shows a popup when new mails arrive.'),
        '0.2.1', 'Razer <razerraz@free.fr, Patrick Ulbrich <zulu99@gmx.net>')
    DEFAULT_CONFIG = {
        'show_account_errors': '1',
        'show_account_name': '1',
        'show_avatar': '1',
        'newest_first': '1',
        'group_mails': '1',
        'max_visible_mails': '10',
        'show_unread_count': '1'}

    def __init__(self):
        self.notification = None
        self.ready = False
        self.thread_lock = threading.Lock()
        self.thread_waitevent = threading.Event()
        self.notify_ready = False
        self.account_map = []
        self.status_map = []
        self.error_accounts = []

    @property
    def available(self):
        return bool(Notify)

    def enable(self):
        self.thread_waitevent.clear()
        self.notify_ready = False
        if not self.ready:
            Notify.init(PACKAGE_NAME.capitalize())
            self.ready = True

    def disable(self):
        self.thread_waitevent.set()

    def load_settings(self):
        self.builder.connect_signals({
            'on_groupmail_changed': self.on_groupmail_changed
        })
        self.builder.get_object('showaccerrors_swt').set_active(
            bool(int(self.config['show_account_errors'])))
        self.builder.get_object('showaccname_swt').set_active(
            bool(int(self.config['show_account_name'])))
        self.builder.get_object('showavatar_swt').set_active(
            bool(int(self.config['show_avatar'])))
        self.builder.get_object('newestfirst_swt').set_active(
            bool(int(self.config['newest_first'])))
        self.builder.get_object('unread_swt').set_active(
            bool(int(self.config['show_unread_count'])))
        self.builder.get_object('groupmail_swt').set_active(
            bool(int(self.config['group_mails'])))
        self.builder.get_object('spn_max_visible').set_value(
            int(self.config['max_visible_mails']))

    def on_groupmail_changed(self, unused_widget, state):
        self.builder.get_object('groupmail_opts_box').set_sensitive(state)

    def save_settings(self):
        self.config['show_account_errors'] = int(self.builder.get_object(
            'showaccerrors_swt').get_active())
        self.config['show_account_name'] = int(self.builder.get_object(
            'showaccname_swt').get_active())
        self.config['show_avatar'] = int(self.builder.get_object(
            'showavatar_swt').get_active())
        self.config['newest_first'] = int(self.builder.get_object(
            'newestfirst_swt').get_active())
        self.config['show_unread_count'] = int(self.builder.get_object(
            'unread_swt').get_active())
        self.config['group_mails'] = int(self.builder.get_object(
            'groupmail_swt').get_active())
        self.config['max_visible_mails'] = int(self.builder.get_object(
            'spn_max_visible').get_value())

    def on_config_saved(self, config):
        if self.main_config.has_section(self._name):
            for name, value in config.items(self._name):
                self.config[name] = value
        self.account_map = list(A.list(config))
        return config

    def update_thread(self, new_mails, unseen_mails):  # pylint:disable=too-many-branches
        with self.thread_lock:
            if not self.notify_ready:
                if not self.wait_ready():
                    return
                self.notify_ready = True
            new_count = len(new_mails)
            body = ''
            summary = ''
            icon = ICON_DEFAULT
            group_mails = int(self.config['group_mails'])
            if int(self.config['show_unread_count']) and not group_mails:
                if len(unseen_mails) > new_count:
                    summary = _('({0} unread mails)').format(len(unseen_mails))
            if int(self.config['newest_first']):
                new_mails = list(reversed(new_mails))
            show_account_name = self.config['show_account_name']
            show_acc = A.count(self.main_config) > 1 and show_account_name
            webmail = None
            for mail in new_mails:
                account = A.get(self.main_config, mail[M.ACCOUNT])
                if account and A.WEBMAIL in account:
                    webmail = account[A.WEBMAIL] or webmail
            if show_acc:
                for mail in new_mails:
                    mail[M.ACCOUNT] = A.get(
                        self.main_config, mail[M.ACCOUNT])[A.NAME]
                new_mails = sorted(new_mails,
                                   key=lambda m: m[M.ACCOUNT].lower())
            for count, mail in enumerate(new_mails, 1):
                if show_acc:
                    summary = f'{mail[M.ACCOUNT]}: {ellipsize(mail.sender)}'
                else:
                    summary = f'{ellipsize(mail.sender)}'
                if group_mails:
                    body += f'{ellipsize(mail.sender)}: '
                    if count == int(self.config['max_visible_mails']):
                        break
                else:
                    body = ''
                if Notification.notify_cap('body-markup'):
                    body += f'<i>{ellipsize(mail[M.SUBJECT])}</i>\n'
                else:
                    body += ellipsize(mail[M.SUBJECT]) + '\n'
                show_avatar = int(self.config['show_avatar'])
                if show_avatar and (not group_mails or count == 1):
                    if mail[M.AVATAR] and os.path.isfile(mail[M.AVATAR]):
                        icon = mail[M.AVATAR]
                if not group_mails or new_count == 1:
                    Notification(
                        summary, body, icon, new_mails).create(
                            self.actions, webmail=webmail)

            if group_mails and new_count > 1:
                summary = _('{0} new mails ').format(new_count) + summary
                if new_count > int(self.config['max_visible_mails']):
                    body += _('and {0} more').format(
                        len(int(new_mails[self.config['max_visible_mails']:])))
                Notification(
                    summary, body, icon, new_mails).create(
                        self.actions, webmail=webmail)

    def on_status_changed(self, status_map):
        if self.status_map and status_map == self.status_map:
            return status_map
        self.status_map = deepcopy(status_map)
        if not int(self.config['show_account_errors']):
            return status_map
        for status in self.status_map:
            uuid = status[A.UUID]
            if status[A.ERR_CODE] <= 0:
                if uuid in self.error_accounts:
                    self.error_accounts.remove(uuid)
                continue
            if status[A.ERR_COUNT] < 3 or uuid in self.error_accounts:
                continue
            self.error_accounts.append(status[A.UUID])
            account = list(filter(lambda a, uuid=uuid: a[A.UUID] == uuid,
                                  self.account_map))
            if not account:
                logging.error(f'Account with uuid {uuid} not found !')
                return status_map
            summary = _('Error on account {0}').format(account[0][A.NAME])
            icon = 'dialog-error'
            Notification(
                summary, status[A.ERR_MSG], icon).create(is_error=True)
        return status_map

    def on_update(self, mails):
        if mails[0]:
            mails_copy = deepcopy(mails)
            threading.Thread(target=self.update_thread,
                             args=(mails_copy[0], mails_copy[1])).start()
        return mails

    def wait_ready(self):
        bus = dbus.SessionBus()
        while not bus.name_has_owner('org.freedesktop.Notifications'):
            self.thread_waitevent.wait(5)
            if self.thread_waitevent.is_set():
                return False
        return True

def ellipsize(string, max_len=DEFAULT_MAXLEN):
    max_len = 10 if  max_len < 10 else max_len
    if len(string) <= max_len:
        return string
    return string[0:max_len - 3] + '...'
