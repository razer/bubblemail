# Copyright 2019 - 2022 razer <razerraz@free.fr>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#
import threading
import logging  # pylint: disable=W0611
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('GLib', '2.0')
# pylint: disable=C0413
from gi.repository import Gtk, GLib

from ..config import PACKAGE_NAME
from ..utils import get_data_file
from ..account import Account as A
from ..dbusservice import CFG_SET_METHOD
from .accountdialog import BACKEND_MAP

EXCLUDED = ('trash', 'sent', 'junk', 'spam', 'drafts', 'outbox', 'templates',
            'archive', 'deleted')

class FoldersMenu:
    def __init__(self, caller, menu_button, account):
        self.caller = caller
        self.account = account
        self.backend = BACKEND_MAP[self.account[A.BACKEND]](self.account)
        self.folder_list = None
        builder = Gtk.Builder()
        builder.set_translation_domain(PACKAGE_NAME)
        builder.add_from_file(get_data_file('folders_menu.ui'))
        builder.connect_signals({
            'on_folder_toggled': self.on_folder_toggled,
            'on_pop_folders_closed': self.on_pop_folders_closed})
        self.widget = builder.get_object
        menu_button.set_popover(builder.get_object('pop_folders'))
        menu_button.set_direction(Gtk.ArrowType.LEFT)

    def show(self):
        if self.folder_list:
            return
        self.widget('pop_folders').set_position(Gtk.PositionType.LEFT)
        if A.is_local(self.account):
            self.folder_list = self.backend.request_folders()
            self.append(self.folder_list, self.account[A.FOLDERS])
            return
        threading.Thread(target=self.get).start()
        self.widget('dialog_folders').hide()
        self.widget('folders_spinner').start()
        self.widget('folders_spinner').show()

    def get(self):
        self.backend.set_proxy()
        self.backend.open()
        self.folder_list = self.backend.request_folders()
        GLib.idle_add(self.append, self.folder_list, self.account[A.FOLDERS])

    def append(self, folder_list, enabled_folders):
        folders_tree = list(map(lambda f: f.split(self.backend.separator),
                            folder_list))
        def not_excluded(folder):
            return not bool(list(
                filter(lambda e: e == folder[0].lower(), EXCLUDED)))
        folders_tree = sorted(filter(not_excluded, folders_tree))
        folders_tree = list(sorted(folders_tree, key=len))
        tree_model = {}

        def get_parent(folder):
            if len(folder) == 1 or '/'.join(folder[:-1]) not in tree_model:
                return None
            return tree_model['/'.join(folder[:-1])]

        if not folders_tree:
            self.widget('folders_spinner').stop()
            self.widget('folders_spinner').hide()
            return

        for tree_level in range(max(map(len, folders_tree))):
            for folder in folders_tree:
                folder_path  = '/'.join(folder)
                if len(folder) < tree_level or folder_path in tree_model:
                    continue
                tree_model[folder_path] = self.widget('tst_folders').append(
                    get_parent(folder),
                    [folder[-1], folder_path in enabled_folders, folder_path])

        self.widget('treeview_folders').expand_all()
        self.set_height(len(folders_tree))
        self.widget('folders_spinner').stop()
        self.widget('folders_spinner').hide()
        self.widget('dialog_folders').show()

    def set_height(self, item_count):
        if not item_count:
            return
        height = item_count * 40
        caller_height = self.caller.window.get_size().height
        height = caller_height if height > caller_height else height
        self.widget('dialog_folders').set_min_content_height(height)

    def on_pop_folders_closed(self, unused_widget):
        self.account.save(create=False, commit=False)
        self.caller.dbus_send(CFG_SET_METHOD,
                              arg=self.account.config.serialize())

    def on_folder_toggled(self, cell, path):
        tst_folders = self.widget('tst_folders')
        folder = tst_folders.get_value(tst_folders.get_iter(path), 2)
        isactive = not cell.get_active()
        tst_folders.set_value(tst_folders.get_iter(path), 1, isactive)
        if isactive and folder not in self.account[A.FOLDERS]:
            self.account[A.FOLDERS].append(folder)
        if not isactive and folder in self.account[A.FOLDERS]:
            self.account[A.FOLDERS].remove(folder)
