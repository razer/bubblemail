# Copyright 2019 - 2022 razer <razerraz@free.fr>
# Copyright 2011 - 2017 Patrick Ulbrich <zulu99@gmx.net>
# Copyright 2016 Timo Kankare <timo.kankare@iki.fi>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#
import os
import threading
import logging  # pylint: disable=W0611
import importlib
from copy import copy
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('GLib', '2.0')
# pylint: disable=C0413
from gi.repository import Gdk, Gtk, GLib

from ..config import PACKAGE_NAME
from ..utils import get_data_file
from ..i18n import _
from ..account import Account as A, RemoteAccount as RA, LocalAccount as LA
from ..localmail import LocalMail
from ..remotemail import RemoteImap, RemotePop3, UNSECURE_PORTS
from ..dbusservice import CFG_SET_METHOD
from ..ui import check_ready

BACKEND_MAP = {RA.IMAP: RemoteImap, RA.POP3:RemotePop3,
               LA.MBOX: LocalMail, LA.MAILDIR: LocalMail}
PROXY_WIDGETS = ('box_pxyurl', 'box_pxyport', 'box_pxytype', 'grd_pxycreds')

class AccountDialog:
    def __init__(self, parent, account=None, config=None, create=False):
        self.parent = parent
        self.account = copy(account)
        self.config = config
        self.create = create
        self.ready = False
        self.backend = None
        self.entry_map = ['ent_name', 'ent_server', 'ent_user', 'ent_pass']
        self.user_edit = []
        self.webmail = False
        self.proxy = False
        self.valid_entries = []
        builder = Gtk.Builder()
        builder.set_translation_domain(PACKAGE_NAME)
        builder.add_from_file(get_data_file('account.ui'))
        style_provider = Gtk.CssProvider()
        style_provider.load_from_path(get_data_file('account.css'))
        Gtk.StyleContext.add_provider_for_screen(  # pylint: disable=no-member
            Gdk.Screen.get_default(), style_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
        builder.connect_signals({  # pylint: disable=no-member
            'on_chk_port_toggled' : self.on_chk_port_toggled,
            'on_chk_wmail_toggled' : self.on_chk_wmail_toggled,
            'on_port_value_changed': self.on_port_value_changed,
            'on_pxyport_value_changed': self.on_pxyport_value_changed,
            'on_pxytype_group_changed': self.on_pxytype_group_changed,
            'on_pxycreds_changed': self.on_pxycreds_changed,
            'on_ent_text_changed': self.on_ent_text_changed,
            'on_ent_focus_changed': self.on_ent_focus_changed,
            'on_localpath_set': self.on_localpath_set,
            'on_chk_proxy_toggled': self.on_chk_proxy_toggled})
        self.widget = builder.get_object
        self.window = Gtk.Dialog(
            'title', self.parent.window, None,
            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
             Gtk.STOCK_OK, Gtk.ResponseType.OK), use_header_bar=True)
        self.window.set_border_width(15)  # pylint: disable=no-member
        self.window.set_default_size(0, 0)
        content = self.window.get_content_area()
        content.add(builder.get_object('dialog_settings'))
        self.window.set_response_sensitive(Gtk.ResponseType.OK, False)
        self.window.set_transient_for(self.parent.window)
        self.load()
        self.ready = True
        self.parent.on_account_edit = True
        self.run()

    @property
    def account_is_valid(self):
        # logging.debug(self.valid_entries)
        # logging.debug(self.entry_map)
        return len(self.valid_entries) >= len(self.entry_map)

    def run(self):
        if self.window.run() == Gtk.ResponseType.OK:  # pylint: disable=no-member
            self.on_ok_clicked()
        else:
            self.close()

    def close(self):
        self.parent.on_account_edit = False
        self.parent.ui_update()
        self.window.close()

    def validate(self):
        if not self.account.save(create=True, commit=False):
            self.account.status[A.ERR_MSG] = _('keyring seems locked')
            self.set_error_widget(sensitive=True)
            return
        self.parent.dbus_send(CFG_SET_METHOD,
                              arg=self.account.config.serialize())
        GLib.idle_add(self.close)

    def load(self):
        self.backend = BACKEND_MAP[self.account[A.BACKEND]](self.account)
        if self.create:
            title = _('New {0} account').format(self.account[A.BACKEND])
        else:
            title = _('Edit {0} account').format(self.account[A.NAME])
        self.window.set_title(title)
        self.widget('ent_name').set_text(self.account[A.NAME] or '')
        if self.account[RA.TYPE] == RA.GOA:
            self.entry_map = []
            for widget in 'box_accname', 'grd_remote', 'box_port':
                self.widget(widget).hide()
            self.widget('exp_advanced').remove(self.widget('box_advanced'))
            self.widget('exp_advanced').destroy()
            self.widget('box_remote').add(self.widget('box_advanced'))
            self.remote_account_sync()
            return
        if A.is_local(self.account):
            self.entry_map = ['ent_name']
            self.widget('box_remote').hide()
            self.widget('grd_localpath').show()
            if self.account[A.BACKEND] == LA.MBOX:
                self.widget('fch_localfolder').hide()
                self.widget('lbl_localpath').set_text(_('Mbox file'))
                self.widget('fch_localfile').set_filename(
                    self.account[LA.PATH] or os.getenv('HOME'))
                self.widget('fch_localfile').show()
            else:
                self.widget('fch_localfolder').set_filename(
                    self.account[LA.PATH] or f'{os.getenv("HOME")}/')
            return
        if self.account[A.BACKEND] == RA.POP3:
            self.widget('spn_port').set_value(995)
        self.widget('chk_proxy').set_sensitive(
            importlib.util.find_spec('socks'))
        if not self.create:
            self.remote_account_sync()

    def remote_account_sync(self):
        self.widget('ent_wmail').set_text(self.account[A.WEBMAIL] or '')
        self.widget('ent_pxyurl').set_text(self.account[RA.PROXY_HOST] or '')
        if self.account[RA.PROXY_PORT]:
            self.widget('spn_pxyport').set_value(
                int(self.account[RA.PROXY_PORT]))
        self.widget('ent_name').set_text(self.account[A.NAME] or '')
        self.widget('ent_server').set_text(self.account[RA.SERVER] or '')
        self.webmail = bool(self.account[A.WEBMAIL])
        if self.webmail and 'ent_wmail' not in self.entry_map:
            self.entry_map.append('ent_wmail')
        self.proxy = bool(self.account[RA.PROXY])
        if self.proxy and 'ent_pxyurl' not in self.entry_map:
            self.entry_map.append('ent_pxyurl')
        for widget in PROXY_WIDGETS:
            self.widget(widget).set_sensitive(self.proxy)
        if self.account[RA.PORT]:
            self.widget('chk_port').set_active(True)
            self.widget('spn_port').set_sensitive(True)
            self.widget('spn_port').set_value(int(self.account[RA.PORT]))
        else:
            self.widget('chk_port').set_active(False)
            self.widget('spn_port').set_sensitive(False)
        self.widget('chk_wmail').set_active(bool(self.account[RA.WEBMAIL]))
        self.widget('box_wmail').set_sensitive(bool(self.account[RA.WEBMAIL]))
        self.widget('chk_proxy').set_active(bool(self.account[RA.PROXY]))
        if self.account[RA.TYPE] != RA.GOA:
            self.widget('ent_user').set_text(self.account[RA.USER] or '')
            self.widget('ent_pass').set_text(self.account[RA.PASS] or '')
            if self.account[RA.PROXY_TYPE] == '4':
                self.widget('rdo_socks4').set_active()
            self.widget('exp_advanced').set_expanded(
                self.account[RA.PORT] or self.webmail or self.proxy)
        for entry_id in self.entry_map:
            if self.widget(entry_id).get_text():
                self.valid_entries.append(entry_id)

    def on_ok_clicked(self):
        self.window.set_response_sensitive(Gtk.ResponseType.OK, False)
        if (not self.account.is_local(self.account)
            and self.account[RA.TYPE] == RA.INTERNAL):
            self.widget('box_status').show()
            self.widget('status_spinner').start()
            self.widget('status_spinner').show()
            self.widget('error_label').hide()
            threading.Thread(target=self.get_remote).start()
        else:
            self.validate()

    @check_ready
    def on_ent_focus_changed(self, _unused_entry, _unused_direction=None):
        for entry_name in self.entry_map:
            entry = self.widget(entry_name)
            context = entry.get_style_context()
            if not hasattr(entry, 'isvalid'):
                entry.isvalid = (True, '')
            if entry_name not in self.user_edit:
                continue
            if entry.has_focus() or entry.isvalid[0]:
                context.remove_class('invalid_entry')
            if not entry.has_focus() and not entry.isvalid[0]:
                context.add_class('invalid_entry')

    @check_ready
    def on_ent_text_changed(self, entry):
        nospace = entry.get_name() not in ('name', 'password')
        printable = entry.get_name() != 'password'
        if self.validate_entry(entry, nospace=nospace, printable=printable):
            self.account[entry.get_name()] = entry.get_text()

    @check_ready
    def on_chk_port_toggled(self, checkbtn):
        toggled = checkbtn.get_active()
        self.widget('spn_port').set_sensitive(toggled)
        self.window.set_response_sensitive(Gtk.ResponseType.OK,
                                           len(self.valid_entries) >= 4)
        if toggled:
            self.account[RA.PORT] = str(int(
                self.widget('spn_port').get_value()))
        else:
            self.account[RA.PORT] = None

    @check_ready
    def on_chk_wmail_toggled(self, checkbtn):
        self.webmail = checkbtn.get_active()
        if self.webmail:
            if 'ent_wmail' not in self.entry_map:
                self.entry_map.append('ent_wmail')
            self.validate_entry(self.widget('ent_wmail'))
        elif 'ent_wmail' in self.entry_map:
            self.entry_map.remove('ent_wmail')
        self.widget('box_wmail').set_sensitive(self.webmail)
        self.window.set_response_sensitive(
            Gtk.ResponseType.OK,
            len(self.valid_entries) >= 4 or self.account[RA.TYPE] == RA.GOA)
        if self.webmail:
            self.account[RA.WEBMAIL] = self.widget('lbl_wmail').get_text()
        else:
            self.account[RA.WEBMAIL] = None
        self.window.set_response_sensitive(Gtk.ResponseType.OK,
                                           self.account_is_valid)

    @check_ready
    def on_port_value_changed(self, spinbtn):
        self.account[RA.PORT] = str(int(spinbtn.get_value()))
        self.window.set_response_sensitive(Gtk.ResponseType.OK,
                                           self.account_is_valid)

    @check_ready
    def on_chk_proxy_toggled(self, checkbtn):
        self.proxy = checkbtn.get_active()
        self.account[RA.PROXY] = int(self.proxy)
        if self.proxy:
            if 'ent_pxyurl' not in self.entry_map:
                self.entry_map.append('ent_pxyurl')
            self.validate_entry(self.widget('ent_pxyurl'))
        elif 'ent_pxyurl' in self.entry_map:
            self.entry_map.remove('ent_pxyurl')
        for widget in PROXY_WIDGETS:
            self.widget(widget).set_sensitive(self.proxy)
        self.window.set_response_sensitive(Gtk.ResponseType.OK,
                                           self.account_is_valid)

    @check_ready
    def on_pxyport_value_changed(self, spinbtn):
        self.account[RA.PROXY_PORT] = str(int(spinbtn.get_value()))
        if not self.account[RA.PROXY_PORT]:
            self.window.set_response_sensitive(Gtk.ResponseType.OK, False)
            return
        self.validate_entry(self.widget('ent_pxyurl'))
        self.window.set_response_sensitive(Gtk.ResponseType.OK,
                                           self.account_is_valid)

    @check_ready
    def on_pxytype_group_changed(self, widget):
        if not widget.get_active():
            return
        if widget.get_name() == 'sock4':
            self.account[RA.PROXY_TYPE] = '4'
        else:
            self.account[RA.PROXY_TYPE] = '5'
        logging.debug(widget.get_name())

    @check_ready
    def on_pxycreds_changed(self, widget):
        self.account[widget.get_name()] = widget.get_text()
        self.window.set_response_sensitive(Gtk.ResponseType.OK,
                                           self.account_is_valid)

    @check_ready
    def on_localpath_set(self, widget):
        self.account[LA.PATH] = widget.get_filename()
        if not self.account[A.NAME]:
            self.account.set_default_name()
            self.widget('ent_name').set_text(self.account[A.NAME])
        self.window.set_response_sensitive(
            Gtk.ResponseType.OK, len(self.valid_entries) >= len(self.entry_map))

    @check_ready
    def validate_entry(self, entry, lmin=3, printable=True, nospace=True):
        self.set_error_widget(show=False, restart_dialog=False)
        entry_name = Gtk.Buildable.get_name(entry)  # pylint: disable=no-member
        if entry_name not in self.user_edit:
            self.user_edit.append(entry_name)
        entry.isvalid = (True, None)
        entry_text = entry.get_text()
        if not entry_text or entry_text.isspace():
            entry.isvalid = (False, _('Entry is empty'))
        if len(entry_text) < lmin:
            entry.isvalid = (False, _('Entry is too short'))
        if printable and not entry_text.isprintable():
            entry.isvalid = (False, _('Entry contain incorrect caracters'))
        if nospace and ' ' in entry_text:
            entry.isvalid = (False, _('Entry contain spaces'))
        if entry.isvalid[0]:
            entry.set_tooltip_text('')
            if entry_name not in self.valid_entries:
                self.valid_entries.append(entry_name)
        else:
            entry.set_tooltip_text(entry.isvalid[1])
            if entry_name in self.valid_entries:
                self.valid_entries.remove(entry_name)
        self.window.set_response_sensitive(Gtk.ResponseType.OK,
                                           self.account_is_valid)
        return entry.isvalid[0]

    def set_error_widget(self, show=True, restart_dialog=True, sensitive=False):
        self.widget('error_label').set_text(self.account.status[A.ERR_MSG])
        self.widget('status_spinner').stop()
        self.widget('status_spinner').hide()
        for widget in 'box_status', 'error_label':
            getattr(self.widget(widget), 'show' if show else 'hide')()
        if show:
            self.window.set_response_sensitive(Gtk.ResponseType.OK, sensitive)
        if restart_dialog:
            self.run()

    def get_remote(self):
        self.account[RA.UNSECURE] = self.account[RA.PORT] in UNSECURE_PORTS
        status = self.account.status
        logging.debug(f'Trying {self.account[RA.BACKEND]} SSL connection on '
                      + self.account[RA.SERVER])

        def _validate():
            account = self.account
            connection_str = 'unsecured' if account[RA.UNSECURE] else 'secured'
            if status.error:
                return False
            logging.debug(f'{account[RA.SERVER]} {connection_str} '
                          + 'connection successful')
            self.backend.close()
            self.validate()
            return True
        self.backend.close()
        self.backend.set_proxy()
        self.backend.open() #(timeout=5)
        if _validate():
            return
        if not self.account[RA.UNSECURE]:
            logging.warning(f'{self.account[RA.SERVER]} SSL connection failed, '
                            + 'trying plain text')
            self.account[RA.UNSECURE] = True
            self.backend.open() #(timeout=5)
            if _validate():
                return
            self.account[RA.UNSECURE] = False
        GLib.idle_add(self.set_error_widget)
