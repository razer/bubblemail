# Copyright 2019 - 2022 razer <razerraz@free.fr>
# Copyright 2013 - 2016 Patrick Ulbrich <zulu99@gmx.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#

# import logging
import gi
# pylint: disable=C0413
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from ..i18n import _
from ..config import PACKAGE_NAME
from ..dbusservice import CFG_SET_METHOD

# pylint: disable=R0903
class PluginDialog:
    def __init__(self, parent, plugin):
        self.parent = parent
        self.plugin = plugin
        self.show()

    def show(self):
        if not self.plugin.ui_file:
            return
        title = _('{0} settings').format(self.plugin.human_name.capitalize())
        dialog = Gtk.Dialog(title, self.parent.window, None,
                            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                             Gtk.STOCK_OK, Gtk.ResponseType.OK),
                            use_header_bar=True)
        dialog.set_resizable(True)
        content = dialog.get_content_area()
        content.set_border_width(6)
        content.set_spacing(6)
        builder = Gtk.Builder()
        builder.set_translation_domain(PACKAGE_NAME)
        builder.add_from_file(self.plugin.ui_file)
        self.plugin.builder = builder
        self.plugin.load_settings()
        content.add(builder.get_object('plugin_settings'))
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.plugin.save(builder=builder)
            self.parent.dbus_send(CFG_SET_METHOD,
                                  arg=self.parent.config.serialize())
        dialog.destroy()
