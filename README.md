# Bubblemail

# An extensible mail notification service

### **Please visit [the project homepage](http://bubblemail.free.fr) for full description, screenshots, and packages download**

# Table of Contents
- [Bubblemail](#bubblemail)
- [An extensible mail notification service](#an-extensible-mail-notification-service)
    - [**Please visit the project homepage for full description, screenshots, and packages download**](#please-visit-the-project-homepage-for-full-description-screenshots-and-packages-download)
- [Table of Contents](#table-of-contents)
  - [Project description](#project-description)
  - [Dependencies](#dependencies)
    - [Build](#build)
    - [Usage](#usage)
    - [Optional dependencies](#optional-dependencies)
  - [Installation instructions](#installation-instructions)
  - [Usage](#usage-1)
    - [Mail collect interval](#mail-collect-interval)
    - [Adding email accounts](#adding-email-accounts)
      - [Remote Imap/pop3 accounts](#remote-imappop3-accounts)
        - [Two factor / Oauth2 authentification](#two-factor--oauth2-authentification)
        - [Advanced settings](#advanced-settings)
        - [Secured protocols usage](#secured-protocols-usage)
    - [Gnome online accounts](#gnome-online-accounts)
    - [Local accounts](#local-accounts)
  - [IMAP and Maildir Folders selection](#imap-and-maildir-folders-selection)
    - [Plugins Tab](#plugins-tab)
  - [Packaging for distributions](#packaging-for-distributions)

## Project description<a name="prjdesc"></a>

Bubblemail is a [DBus](https://en.wikipedia.org/wiki/D-Bus) service providing a list of the new and unread user's mail from:

 * Local inbox (mbox and maildir formats)
 * POP3 and/or IMAP servers defined as internal accounts
 * Gnome online accounts with email support (Google, Microsoft exchange, IMAP and SMTP)

It's desktop independent, highly configurable, can be extended via plugins, and is mostly written in python.

Currently, two different frontends can be used to provide user notifications:

  * Notifications using libnotify, implemented directly in the project as a plugin
  * [The gnome desktop specific indicator](https://framagit.org/razer/bubblemail-gnome-shell)

Bubblemail provides a set of default plugins for:

 * Sound notifications
 * Script execution
 * Spam filtering
 * Libnotify notifications

If you're interested in how plugins are made, you can look at the [Plugin Developement Guide](https://framagit.org/razer/bubblemail/blob/master/CONTRIBUTING.md#plugwrite) on the [Contributing page](https://framagit.org/razer/bubblemail/blob/master/CONTRIBUTING.md)

This project started as a fork of the Mailnag project. Since then, almost all the code has been rewritten, but all the references to the [Mailnag's original developers](https://framagit.org/razer/bubblemail/blob/master/AUTHORS) have been kept.

## Dependencies<a name="depends"></a>

### Build
*  **python >= 3.5**
*  [python-setuptools](https://github.com/pypa/setuptools)
*  [python-pillow](https://python-pillow.org/)
*  [vala](https://wiki.gnome.org/Projects/Vala/Documentation)
*  [folks](https://wiki.gnome.org/Projects/Folks)

### Usage
*  **python >= 3.5**
*  [python-setuptools](https://github.com/pypa/setuptools)
*  [python-dbus](https://www.freedesktop.org/wiki/Software/dbus/)
*  [python-gobject](https://pygobject.readthedocs.io/en/latest/)
*  [libsecret](https://wiki.gnome.org/Projects/Libsecret)
*  [gnome-keyring](https://wiki.gnome.org/Projects/GnomeKeyring)

### Optional dependencies

* [python-pysocks](https://github.com/Anorov/PySocks) providing per account proxy setup
* [gnome-online-accounts](https://wiki.gnome.org/Projects/GnomeOnlineAccounts) (needed for two-factor authentification / oauth2 providers support)
* [folks](https://wiki.gnome.org/Projects/Folks) used for getting avatar pictures

## Installation instructions<a name="install"></a>

Bubblemail uses [setuptools](https://pypi.org/project/setuptools/).

To install it from this repo:

```sh
git clone https://framagit.org/razer/bubblemail.git
cd bubblemail/
sudo python3 setup.py install
```

The installation default prefix `/usr` can be changed with the `--prefix` command line option:

```sh
git clone https://framagit.org/razer/bubblemail.git
cd bubblemail/
sudo python3 setup.py install --prefix=/usr/local
```
For package generation, installation root folder can be provided with the `--root` command line option:

```sh
git clone https://framagit.org/razer/bubblemail.git
cd bubblemail/
python3 setup.py install --root=/tmp/pkg
```

It may be necessary to add setuptools's `--old-and-unmanageable` option to avoid egg generation (see [this bug](https://framagit.org/razer/bubblemail/-/issues/52))

```sh
sudo python3 setup.py install --old-and-unmanageable --prefix=/usr/local
```


## Usage<a name="usage"></a>

Bubblemail comes in two parts:

 * A service, loaded on desktop login, collects the mails and provides the dbus service
 * A settings interface, using Gtk, providing the user's frontend.

Bubblemail service can be run from the command line:
```sh
bubblemaild
```

Bubblemail settings interface can be run from the command line:
```sh
bubblemail
```

Note: running settings (ie: using `bubblemail` command) will try to get the service running if it's not.

### Mail collect interval<a name="collival"></a>
The interval between accounts checking can be changed in the "General" Tab, and be set to "never" to disable checks

### Adding email accounts<a name="addacc"></a>
Bubblemail supports 3 specific types of email accounts :
* Remote Imap/pop3 accounts
* Local Maildir/mbox accounts
* Gnome online accounts with email support (Google, Microsoft exchange, IMAP and SMTP)

Remote and local accounts need to be named in the account add window, among other specific setting.
If you don't know how to set up your own email account and you doesn't want to spend time on it, Gnome online account should be made for you, since it doesn't require some much knowledge.

#### Remote Imap/pop3 accounts<a name="remacc"></a>
Bubblemail supports both Imap and Pop3 protocols. If your provider supports Imap, it may be the best choice since it's a more popular and advanced protocol.

In both cases, you will need an url for the server to use. Most of the time, this url begins with the name of the protocol (ie imap or pop3), followed by the main domain name of the provider. For example :

* imap.google.com
* imap.mail.yahoo.com
* imap.gmx.com

Of course, you will need some credentials with an username and a password. It should be the same as the webmail service.

##### Two factor / Oauth2 authentification
**Some providers, like gmail, proposes two factor identification by default, forcing user to validate authorisation using a web service**. Since Bubblemail doesn't integrate a webrowser, this task have to be done elsewhere, for instance using [Gnome online accounts](#goacc).

Besides, most email providers offers an option to disable oauth2. Please follow your provider specific method in order to use bubblemail without using [Gnome online accounts](#goacc).

##### Advanced settings

For each account, a ***custom tcp port*** can be provided. It's sometime usefull to use exotic ports to access your self provided services behind firewalls. If this setting stays empty, protocol's default tcp ports are used. By default, secured protocols are used by default for accounts, plain text is used if it fails. By specifying a port containing ***143*** (imap accounts) or ***110*** (pop accounts), unsecured protocol will be used by default. ***It seems to be a turnaround to get protonmail accounts working (see issue #10 and #29)***

By default, when an email is clicked from the ***libnotify notification plugin*** or the [The gnome desktop specific indicator](https://framagit.org/razer/bubblemail-gnome-shell), bubblemail will open the default mail application from your desktop settings. If you read and write emails using a webservice like gmail, you can toggle the ***open mail in the browser*** option, and set the url of this webservice to have it opened when you click on an email.

A ***proxy server*** can be set for each account, by providing ***IP address or URL*** and a ***tcp port***. For instance, using a local instance of [TOR](https://www.torproject.org/), IP should be set to 127.0.0.1 and port to 9050. If the proxy needs credentials, fill them in the user/password inputs.

##### Secured protocols usage
For each remote protocols, both secured version (using SSL) and plain text are available. As most of the mail services provides the secured version of the protocol, bubblemail will use it by default, and revert back to the plain text protocol in case of failure. User is informed of an unsecure protocol usage with an icon in the account list.

### Gnome online accounts<a name="goacc"></a>
Bubblemail periodicaly checks for gnome online accounts with email support (Google, Microsoft exchange, IMAP and SMTP), and syncs its accounts following user changes. Folders can be selected in the account editing window.

### Local accounts<a name="locacc"></a>
You can add local mail accounts using the most common protocols : ***Mbox*** and ***maildir***. For each, a path on the computer must be provided.

At first run, Bubblemail will check the presence of a user's mbox file or Maildir folder, and adds local mail accounts in the settings interface accordingly.

## IMAP and Maildir Folders selection<a name="foldsel"></a>
Once an Imap or maildir account is added, you can select the folders you want new emails notifications from using the ***Subscribed folders*** button. By default, only the ***Inbox*** folder is subscribed.

Note : since Pop3 and mbox protocols doesn't support folders, subscribed folders selection is not available for them.

### Plugins Tab<a name="plugtab"></a>

Plugins can be enabled / disabled using the plugins tab. Plugin options can be changed for the ones providing it.
Bubblemail checks for plugins:

* In the user's config folder: ~/.config/bubblemail/plugins
* In the system-wide python site-packages installation folder

## Packaging for distributions<a name="packaging"></a>
Generate RPM (Fedora, Redhat Enterprise):
```sh
python3 setup.py bdist_rpm
```
Generate Debian package (Ubuntu, Mint):
```sh
python3 setup.py --command-packages=stdeb.command sdist_dsc
cd deb_dist/bubblemail-<version>
cp ../../packaging/debian/control ../../packaging/debian/copyright debian/
debuild -i -us -uc -b
```
