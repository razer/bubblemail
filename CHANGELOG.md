# Changelog
## [v1.9] - 2024-05-26
 - Increase max visible characters for email fields in libnotify (#3d4c64c5)
 - Improve translations (#0371e847)
 - Improve remote mails handling and decoding (#327bd57f, #fb831378, #54725675, #710476f7, #799cb9f3)
 - Fix bug for config options in libnotify plugin (#df776343)
 - Avoid race condition issue from libnotify plugin (#fc3e963d)
 - Prevent UI exception showing empty folders tree (#1cc8f7fd)

## [v1.8] - 2022-06-17
 - Implement per account socks proxy support (#28fbbbe0, #787b9ed2, #642ac941, #57127195, #57127195, #0255ee9f, #6a1a6089, #694af182, #830ab958, #84d220e4, #5ad60da0)
 - Switch from distutils to setuptools (#49613784)
 - Use slash as default imap folder separator (#6f895a4f)
 - Fix bug #33 : Goa account config not saved/synced (#3d0b2b5a)

## [v1.7.1] - 2022-05-11
 - Fix bug: use correct separator for Imap/Maildir folders (#1fdb6804, #d598e212)
 - Convert list config options to JSON format (#f89b0cf0)

## [v1.7] - 2022-05-01
 - Fix bug: failure on account creation with unsecured protocol (#29) (cd8559ae)
 - Fix bug: account folders tree in user interface not always reflecting actual config state (#31) (ef8d9d03)
 - Sync GOA accounts without delay on fresh install (#31) (ca664d09)
 - Use json serialisation to store and retrieve lists in the config file (#31) (ef8d9d03)
 - Use sysconfig instead of deprecated distutils for retrieving python modules path (2a9143a6)

## [v1.6] - 2022-02-26
 - Add support for exchange gnome online accounts with mail support (af3b2198, bafe788f, cb769a09, 2ff942bf)
 - Improve error handling when keyring is locked (e8c18624)

## [v1.5.1] - 2021-11-21
 - Fix bug: exception not catched for mail without recipient
 - Fix spamfilter ui bug (#24) : case sensitive widget always active
 - UI enhancement (#24): make plugin window resizable

## [v1.5] - 2021-11-10
 - Fix folders count label : right way around (68bba95a)
 - Improve spam filter plugin (ab8108fc) (#24)
 - Use ldd instead of ldconfig to determine libfolks availabilty (e82fce50) (#21)
 - Fix bug: misuse of imap server's hierarchy separator (5e72b884)
 - Improve account settings drawing for gnome online accounts (e4ab5bdd)
 - Wait 10 fetchs before removing dismissed items from cache (e7e2e957)
 - Add high definition icons (6fa17f5d)

## [v1.4] - 2021-04-11
 - Allow spaces in account passwords (81b61188) (#17)
 - Add account specific option for webmail usage (71487b6c, 45161c2e, 81b61188) (#16)
 - Improve exception catching and server backend detection (6b53336f)

## [v1.3] - 2020-10-17
 - Fix bug: Protonmail provider ports not well handled (#17e7a380, #dadc4cf7) (#10)
 - Fix bug: Avoid recursive timeouts during fetch, canceling process on first timeout (#dc017c35)
 - UI: set log page scroll to end position (#11)
 - Sound plugin: Honor "Do Not Disturb" mode (#fb48a145, #6715a126) (#12)

## [v1.2] - 2020-08-27
 - Fix bug: RecursionError on GOA collect when offline or locked (see #9) (11b62aed)
 - Fix bug: Avoid error reporting on Goa accounts when offline state (2247ef36)
 - Fix bug: libnotifyplugin dismiss action not working for a single mail (569372eb)
 - Fix bug: get length of potentially null object (60ff03b0)
 - New error handling implementation :
   - Previously, accounts status were stored in the config object and file, leading to a bunch of saving calls and hook/dbus signal trigerring for something unrelated to user's conf.
   - Now, specific storage, hook, dbus method and signal have been added for account status.
   - Besides, a new error counter allow plugins and extension to wait for multiple similar error before reporting it to the user, which is already done in libnotifyplugin and the gnome-shell extension
 - Use Alexey's icon - Improve about dialog (0a69a292)
 - Update ui files to Gtk+3.22 - Use icon ressource name (0de283d9)
 - Improve ui behavior on window resize (108413e4)
 - Improve handlings on timeouts (9fc660d3, 25c3d29f)
 - Impove Greek translation (10432900)
 - Catch exception on pwd based GOA accounts without credentials (ea3e0d6b)

## [v1.1] - 2020-06-17
 - Fix crash: 'unpack' called on NoneType content for malformed server responses (0d79f169)
 - Fix crash on bad malformed imap responses for folder list: exclude 'inbox' from being checked (feee2290, 12d8e1d8)
 - Fix bug: Password based Gnome online accounts always considered locked despite valid credentials (dd4593ca)
 - Fix bug: mails on unsecure accounts are restored from cache if there are no new mails (4fc3aa36)
 - Fix bug: Timeouts on open() method not catched (7c77ecfd)
 - Fix bug: Unsecured servers dropping SSL connections attempts considered as timed out despite working well using plain (33561eb2)
 - Error handling improvement, unsecured context not considered as an error anymore, code refactoring (2d0466b5, 5accd031, 17d53f84, 4d95a613)
 - Consider DNS server failure as an offline state instead of an error (c68b7847)
 - UI: Add warning icon for unsecured accounts (79db0c87)
 - UI: Show specific stack with add account buttons if no accounts found (d0d6d02c)
 - UI: Dissociate scrolling between accounts and plugins stack pages (3f0d646e)
 - UI: Show auth warning dialog only on imap account creation (3e0308e2)
 - Account creation/edition: Decrease timeout delay to improve reponsiveness (3e017469, 33561eb2, 4aaa9ebd)
 - Account creation/edition: Try plain text if secure connexion goes timeout (33561eb2)
 - Bypass secure connection try if port setting suggest it with plain text protocols (110 and 143) (732abb77)
 - Bypass secure connection try during fetch if account have been flagged as unsecure (3e017469)
 - Improve mails fetch scheduler implementing control on it, every manual refresh reset the timer (6e1449d1)
 - Listen for systemd login suspend/resume dbus signals if available, reset the scheduler before suspend, check for mails on resume (428bfd20)
 - Limit connexion errors on boot, waiting a little before fetching mails, giving connexion manager time to properly start (78f7388f)

## [v1.0] - 2020-05-23
 - Fix crash on account validation failure (e74a25ef)
 - Fix crash on socket timed out connection close (6180cbec)
 - Fix crash on some malformed imap responses (05adaf8f)
 - Keep message list and dismiss states on service restart (60bed3d9)
 - Cancel mail account collect when timeout is reached (3149486f)
 - Improve error handling (5518012e, 9c7f228a, 989fee73, 789e0c0d, 1e50bb41)
 - Libnotify plugin: add feature to notify about account errors (270b82a8, f77301a4, 13644363)
 - Libnotify plugin: Fix crash dismissing mails already flagged as read or deleted (607d469f)
 - Improve about dialog (ab4c7c61, 3fc47036)
 - Get rid of icons added to dialogs on debian (2eeaed64)
 - Add man pages (ada7bbe3)
 - Improve italian translation (thanks to Alyssa Cohen) (1f3418bb)

## [v0.7] - 2020-04-14
 - Add folder selection button directly on account row (221f4542, d064bce1, ab3ac138, 7e77f1e6, 620985ea, d0f9ce11)
 - Show folders as a tree (d869ba7e, 33d1f163)
 - Decode Imap folders in native charset (2ad7c49e)
 - Add show account name option to libnotify plugin (59d40397)
 - Improve connexion errors handling (6ddd8664)
 - Improve developement mode providing home folder user's choice (eda180de)
 - Fix bug: non printable caracters must be allowed for passwords (14413593)
 - Fix bug: Undoing account delete leading to missing account settings (e88d4216)
 - Fix bug: crash loading gnome online account without credentials (2adc6c90)
 - Fix bug: New account not always added
 - Fix bug: Libnotify plugin is abnormaly changing mail list items (8b646e63)
 - Fix bug: folders selection on local maildir account not working (7e705572)

## [v0.6] - 2020-03-30
 - Remove version checker (packaging for all distribution is done, no need for it anymore) (10638011)
 - Stop account mail collect on first encoutered timeout (0209d0a8)
 - Add greek translation, credits to Alexander Ploumistos (8038e4c8)
 - Improve error handling (4c4c21ac, 2c50afcc, 3df11705, 896e8165)
 - Improve various email format support (3dcbbed7, 75dde255)
 - Improve reliability and exceptions handling (e810e932, 27e7bc98, bca84c50, 83e6ee46, 4d08cbaf, 812eccd1)
 - Allow spaces in account names (cc01f6b6)
 - Save config only on mutation (7da59d92)
 - Fix bug: adding pop3 account not working (1c6be89f)
 - Fix bug: Gtk interface not always refreshed (4bedd4a7)
 - Extend test units cases providing problematic raw emails (b719f0a8)

## [v0.5] - 2020-02-19
 - Libnotify plugin: Add per mail notification feature (2953720e)
 - Keep previous collect results on remote account failure (3acf4982)
 - Save and restore main window size (830c87ef, e857a923 and 358c8f26)
 - Implement first run auto discovering of local mail sources (040b241c)
 - Improve local mail support providing multiple local accounts (6c07190b)
 - Get better account creation/edition coherence avoiding backend changes (59d8652d, 72304c7e)
 - Get better data file tree coherence moving plugins ui files to data folder(79302ec5, 0f25f90d)
 - Improve legacy compatibility providing better appdata.xml content and path (8a97143c, a69f744e and 4b584a3f)
 - Produce debug output during vala build (3e25109a)
 - Bug fixes

## [v0.4] - 2020-01-15
 - Rename binaries and desktop files with simplicity in mind (a3d1d59e, 3694b860)
   - Service, formely `bubblemail-service` become `bubblemaild`
   - Settings, formely `bubblemail-settings` become `bubblemail`
 - Add new version checker using gitlab api, inform user for new releases via a dialog (4adfd984)
 - Avoid avatar generation error if libfolks not installed, checking for it's installation at first (f842675a 4adfd984)
 - Fix bug providing avatar file that doesn't exist anymore, revert to default ones in this case (f842675a)
 - Fix bug dismissed mails are forgotten in case of account connexion error between checks (051d364a)
 - Fix bug: undo after account deletion not working (62bd432f)
 - Fix bug: Locale po files outdated (ec0e5b08)
 - Various clean ups

## [v0.3] - 2019-12-16
 - Add default colorized avatars based on sender name or address (c9c6c09b bcec0a2c)
 - Improve remote mail error reporting (c61ad860)
 - Add insecure connection warning (023c925c)
 - Add reverse order option for libnotify plugin (c9c6c09b)
 - Add config version property & check for config file incompatibity (4ca83d37)
 - Add dbus method & signal providing config (406c8496)
   - Used by gs extension to check version compatibility and account error reporting
 - Use dbus string array signature everywere (a9fe72d1)
 - Use account uuid instead of account name for mail to account relationship (a9fe72d1)
 - Use xdg cache folder instead of config for logs and caches (10c0b4c0)
 - Use pickle for mail cache (10c0b4c0)
 - Fix bug: version not always checked (fdfa357e)
 - Various clean ups